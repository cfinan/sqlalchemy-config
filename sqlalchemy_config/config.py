"""Manage SQLAlchemy configuration options that are stored in a config file.
"""
from stat import S_IROTH, S_IWOTH, S_IRGRP, S_IWGRP
from sqlalchemy.exc import ArgumentError
from sqlalchemy import orm
import sqlalchemy_utils
import urllib.parse
import sqlalchemy
import configparser
import os
from warnings import warn


DB_CONFIG = "DB_CONFIG"
"""The default environment variable name (`str`)
"""
DEFAULT_CONFIG = None
"""The default config location (`NoneType`)
"""

# Here are some useful variables for SQLAlchemy
try:
    DEFAULT_CONFIG = os.environ[DB_CONFIG]
    """Look for the environment variable (`str`)
    """
except KeyError:
    try:
        # No ENV variable, try HOME
        DEFAULT_CONFIG = "~/.db.cnf"
        """The default fallback path for the config file, root of the home
        directory (`str`)
        """
    except KeyError:
        # HOME environment variable does not exist for some reason
        pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_new_sessionmaker(dbarg, conn_prefix="db.", config_arg=None,
                         config_env=None, config_default=None,
                         case_sensitive=False, exists=False,
                         raise_on_exists=False,
                         db_file_extensions=[".db", ".duckdb"],
                         **kwargs):
    """This essentially wraps all of the other functions into a single
    function and handles the logic of a url being supplied via a cmd-line.

    Parameters
    ----------
    dbarg : `str`
        Either a full SQLAlchemy database URL, or a section heading in a
        config file, or a path to a SQLite/duckdb database.
    conn_prefix : `str`, optional, default: `db.`
        The prefix for a config connection parameter under the section
        heading. All connection keys should be prefixed with this.
        i.e. `db.url`.
    config_arg : `str` or `NoneType`, optional, default: `NoneType`
        The path to the config file that has been potentially supplied by a
        command line argument. If not supplied then ``config_env`` will be
        checked.
    config_env : `str` or `NoneType`, optional, default: `NoneType`
        The path to the config file that has been potentially supplied by an
        environment variable.
    config_default : `str` or `NoneType`, optional, default: `NoneType`
        A default fallback path to the config file.
    case_sensitive : `bool`, optional, default: `False`
        Should the config file be opened in case sensitive mode?
    exists : `bool`, optional, default: `False`
        If the ``url_arg`` is supplied then the database must exist or a
        FileNotFound error will be raised. If this is False and the database
        exists then no error is raised unless raise_on_exists is True.
    raise_on_exists : `bool`, optional, default: `False`
        This alters the behavior or exists=False, normally this is indifferent
        if the database exists. However, if raise_on_exists is True and
        exists=False then a FileExists error will be raised.
    db_file_extensions : `list` of `str`, optional, \
    default: `[".db", ".duckdb"]`
        The file extensions that indicate a ``dbarg`` file path is a file based
        database.
    **kwargs
        These are supplied to ``sqlalchemy_config.config.read_config_file``.

    Returns
    -------
    sessionmaker : `sqlalchemy.Sessionmaker`
        An object to make SQLAlchemy sessions. The engine has been bound to
        this.

    Raises
    ------
    ValueError
        If all of the potential input paths are ``NoneType``.
    FileNotFoundError
        If a valid config file is not located, this function only checks that
        the file path is valid. It does not actually check if the config
        file has the expected sections and keys. This is also raised if a
        database URL is supplied and the database does not exist.
    PermissionError
        If the config file can be read by other people.

    Notes
    -----
    If you want an easy function to call that will do everything, then use
    this. This supersedes ``sqlalchemy_config.config.get_sessionmaker``.
    If you want more control then use the other component functions in
    this package.

    The hierarchy for getting the connection parameters is ``dbarg``,
    ``config_arg``, ``config_env`` and ``config_default``.
    """
    sm = None
    try:
        # Try to parse it as an SQLAlchemy URL string
        saurl = sqlalchemy.engine.url.make_url(dbarg)
        sa_engine = sqlalchemy.create_engine(saurl)
        sm = orm.sessionmaker(bind=sa_engine)
    except ArgumentError:
        # Not a URL string, so must be a file (i.e. SQLite/duckdb)
        # or a config section. First check for config file/section
        try:
            config_file = get_config_file(
                arg=config_arg, env=config_env, default=config_default
            )

            # Check and parse the config file
            check_config_permissions(config_file)
            cfg = read_config_file(
                config_file, case_sensitive=case_sensitive, **kwargs
            )

            conn_args = get_sqlalchemy_args(cfg, dbarg, conn_prefix)
            sm = get_sqlalchemy_conn(conn_args, conn_prefix)
        except (ValueError, FileNotFoundError, KeyError):
            # No config file
            # If we get here then it must be a database file
            try:
                # If it errors we assume SQLite database file
                saurl = sqlalchemy.engine.URL.create(
                    database=dbarg, drivername='sqlite'
                )
            except AttributeError:
                saurl = sqlalchemy.engine.url.URL(
                    'sqlite', database=dbarg
                )

            sa_engine = sqlalchemy.create_engine(saurl)
            sm = orm.sessionmaker(bind=sa_engine)

    db_exists = database_exists(sm)

    if exists is True and db_exists is False:
        raise FileNotFoundError(
            "Database is expected to exist but does not exist"
        )
    elif exists is False and raise_on_exists is True and db_exists is True:
        raise FileExistsError(
            "Database exists but is not expected to exist"
        )
    return sm


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_sessionmaker(conn_section, conn_prefix, url_arg=None,
                     config_arg=None, config_env=None, config_default=None,
                     case_sensitive=False, exists=False, **kwargs):
    """Get a sessionmaker from a supplied arguments or a config file. This is
    deprecated, please use ``sqlalchemy_config.config.get_new_sessionmaker`` .

    Parameters
    ----------
    conn_section : `str`
        The section heading in the config file containing the connection
        information.
    conn_prefix : `str`
        The prefix for a config connection  parameter under
        ``conn_section``.
    url_arg : `str` or `NoneType`, optional, default: `NoneType`
        An SQLAlchemy URL string (or file name if SQLite) that may have been
        given at the commend line. If ``NoneType`` then not supplied.
    config_arg : `str` or `NoneType`, optional, default: `NoneType`
        The path to the config file that has been potentially supplied by a
        command line argument.
    config_env : `str` or `NoneType`, optional, default: `NoneType`
        The path to the config file that has been potentially supplied by an
        environment variable.
    config_default : `str` or `NoneType`, optional, default: `NoneType`
        A default fallback path to the config file.
    exists : `bool`, optional, default: `False`
        If the ``url_arg`` is supplied then the database must exist or a
        FileNotFound error will be raised.
    **kwargs
        These are supplied to ``sqlalchemy_config.config.read_config_file``.

    Returns
    -------
    sessionmaker : `sqlalchemy.Sessionmaker`
        An object to make SQLAlchemy sessions. The engine has been bound to
        this.

    Raises
    ------
    ValueError
        If all of the potential input paths are ``NoneType``.
    FileNotFoundError
        If a valid config file is not located, this function only checks that
        the file path is valid. It does not actually check if the config
        file has the expected sections and keys. This is also raised if a
        database URL is supplied and the database does not exist.
    PermissionError
        If the config file can be read by other people.
    """
    warn(
        "get_sessionmaker is deprecated, please use get_new_sessionmaker"
        " instead", DeprecationWarning, stacklevel=2
    )

    # Supplied a file or URL on the command line
    if url_arg is not None:
        try:
            # Try to parse it as an SQLAlchemy URL string
            saurl = sqlalchemy.engine.url.make_url(url_arg)
        except ArgumentError:
            try:
                # If it errors we assume SQLite database file
                saurl = sqlalchemy.engine.URL.create(
                    database=url_arg, drivername='sqlite'
                )
            except AttributeError:
                saurl = sqlalchemy.engine.url.URL(
                    'sqlite', database=url_arg
                )

        sa_engine = sqlalchemy.create_engine(saurl)
        sm = orm.sessionmaker(bind=sa_engine)

        if exists is True and database_exists(sm) is False:
            raise FileNotFoundError(
                "Database URL is supplied but database does not exist."
            )
        return sm

    config_file = get_config_file(
        arg=config_arg, env=config_env, default=config_default
    )

    # Check and parse the config file
    check_config_permissions(config_file)
    cfg = read_config_file(
        config_file, case_sensitive=case_sensitive, **kwargs
    )

    # Get the required section and create a sessionmaker from it
    # section = get_section_data(cfg, conn_section)
    conn_args = get_sqlalchemy_args(cfg, conn_section, conn_prefix)
    return get_sqlalchemy_conn(conn_args, conn_prefix)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def database_exists(sm):
    """A wrapper around ``sqlalchemy_utils.database_exists``, that has a
    sessionmaker parameter rather than URL. The URL is extracted from the bind.

    Parameters
    ----------
    sm : `sqlalchemy.Sessionmaker`
        An object to make SQLAlchemy sessions. The connection engine should be
        bound to this.

    Returns
    -------
    exists : `bool`
        ``True`` if the database exists ``False`` if not.

    See also
    --------
    sqlalchemy_utils.database_exists
    """
    session = sm()
    saurl = session.bind.url
    return sqlalchemy_utils.database_exists(saurl)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_db(sm, exists=False):
    """Make sure that the database provided by the sessionmaker exists.

    Parameters
    ----------
    sm : `sqlalchemy.Sessionmaker`
        An object to make SQLAlchemy sessions. The connection engine should be
        bound to this.
    exists : `bool`, optional, default: `False`
        Should we expect that the database exists? If so and it doesn't then an
        error is raised.

    Raises
    ------
    FileExistsError
        If exists is ``False`` and the database exists
    FileNotFoundError
        If exists is ``True`` and the database does not exist

    Notes
    -----
    If this runs successfully then the user can be sure that the database
    actually exists.
    """
    session = sm()
    saurl = sm().bind.url

    if exists is False:
        if sqlalchemy_utils.database_exists(saurl):
            raise FileExistsError(
                "database exists: {0}".format(saurl.database)
            )
        sqlalchemy_utils.create_database(saurl)
    else:
        if sqlalchemy_utils.database_exists(saurl) is False:
            raise FileNotFoundError(
                "database does not exist: {0}".format(saurl.database)
            )
    session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_config_file(arg=None, env=None, default=None):
    """Test to see if the config file exists, if it does return a config object
    if not then raise an error.

    Parameters
    ----------
    arg : `str` or `NoneType`, optional, default: `NoneType`
        The path to the config file that has been potentially supplied by a
        command line argument.
    env : `str` or `NoneType`, optional, default: `NoneType`
        The path to the config file that has been potentially supplied by an
        environment variable.
    default : `str` or `NoneType`, optional, default: `NoneType`
        A default fallback path to the config file.

    Returns
    -------
    config : `configparser.ConfigParser`
        The config file parser object.

    Raises
    ------
    ValueError
        If all of the potential input paths are ``NoneType``.
    FileNotFoundError
        If a valid config file is not located, this function only checks that
        the file is a valid ini file. It does not actually check if the config
        file has the expected sections and keys.

    Notes
    -----
    This just handles the logic of possible sources for the config file path.
    The config file location is set to the default locations (for example this
    could be a hidden file in the root of the home directory). The ``arg`` is
    checked first, if it is set then it is used as the source for the config
    file. If ``arg`` is not set, then the environment variable is checked and
    used if defined. The config path that was set from the path above is then
    checked to see if it can be opened.
    """
    # Make sure some values have been given
    if all([i is None for i in [arg, env, default]]) is True:
        raise ValueError("no config file locations to search")

    # Initialise with the default
    cfg = default

    # If a cmd-line arg has been supplied, set it
    if arg is not None:
        cfg = arg
    else:
        try:
            # No cmd-line arg so we see if there is an environment
            # variable
            cfg = os.environ[env]
        except (KeyError, TypeError):
            # TypeError is env is None
            pass

    try:
        # Attempt to open to preempt any errors - as I do not think confg
        # parser errors out
        open(cfg).close()
    except (TypeError, FileNotFoundError):
        # Could either not exist or be NoneType
        raise FileNotFoundError(
            "can't find configfile: arg:{0};env_var:{1};default:{2}".format(
                arg, env, default
            )
        )
    return cfg


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_config_file(config_file, case_sensitive=False, **kwargs):
    """Open the config file and return the configparser object.

    Parameters
    ----------
    config_file : `str`
        The path to the config file.

    Returns
    -------
    config : `configparser.ConfigParser`
        The config parser object to interact with the config file.
    """
    # Sort out relative paths
    config_file = os.path.realpath(os.path.expanduser(config_file))

    # Attempt to open to preempt any errors - as I do not think confg
    # parser errors out
    open(config_file).close()

    # Open up and return
    config = configparser.ConfigParser(**kwargs)
    if case_sensitive is True:
        config.optionxform = str

    config.read(config_file)
    return config


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def check_config_permissions(config_file,
                             perms=S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH):
    """Make sure that the config file is only readable by you.

    Parameters
    ----------
    config_file : `str`
        The path to the config file.
    perms : `int`, optional, default: `54`
        The permissions that the config file is NOT allowed to have. This
        defaults to being readable by only the user.

    Raises
    ------
    PermissionError
        If the config file can be read by other people.
    """
    # Sort out relative paths
    config_file = os.path.realpath(os.path.expanduser(config_file))

    st = os.stat(config_file)
    if bool(st.st_mode & (S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH)) is True:
        raise PermissionError("Ensure config file has correct permissions")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_section_data(config, section):
    """Return a section from the config file (as a dict).

    Parameters
    ----------
    config : `configparser.ConfigParser`
        The config parser object to interact with the config file.
    section : `str`
        The section to return.

    Returns
    -------
    config_section : `dict`
        The config parser object to interact with the config file.

    Raises
    ------
    KeyError
        If the config section is not defined.
    """
    try:
        # If we get here then no file is defined and we are relying on a
        # file being defined in the config file
        return dict(config[section])
    except KeyError as e:
        # No section defined
        raise KeyError("section not defined: '{0}'".format(section)) from e


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_sqlalchemy_args(config, section, prefix):
    """Attempt to get a connection to the database.

    Parameters
    ----------
    config : `configparser.ConfigParser`
        The config file parser object.

    Returns
    -------
    config_args = `dict`
       Configuration arguments that can be passed to
       `sqlalchemy.engine_from_config`.

    See also
    --------
    sqlalchemy.engine_from_config
    """
    section = get_section_data(config, section)

    # Any DBAPI specific args
    connect_args = {}

    # SQLAlchemy config args
    config_args = {}

    for k, v in section.items():
        # If we are prefixed then it is an arg to SQLAlchemy, otherwise it is
        # an arg to the DBAPI
        if k.startswith(prefix):
            config_args[k] = v
        else:
            connect_args[k] = v

    # Make sure we add the connect args if we have them
    if len(connect_args) > 0:
        config_args['connect_args'] = connect_args

    return config_args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_sqlalchemy_conn(conn_args, conn_prefix):
    """Get a SQLAlchemy Sessionmaker for the database, this is bound to an
    engine that is created from the connection information.

    Parameters
    ----------
    conn_info : `dict`
        The connection parameters to SQLAlchemy that have been read in from a
        config file.
    conn_prefix : `str`
        The connection prefix for parameters in the ``conn_info`` dict.

    Returns
    -------
    sessionmaker : `Sessionmaker`
        An SQLAlchemy sessionmaker object.

    See also
    --------
    sqlalchemy.engine_from_config
    """
    # create the database engine, note that this does not connect until it is
    # asked to do something, so we use sqlalchemy_utils to enforce the
    # must_exist
    engine = sqlalchemy.engine_from_config(conn_args, prefix=conn_prefix)

    # If we get here then we are good to continue, so we create a session
    return sqlalchemy.orm.sessionmaker(bind=engine)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def encode_password(pwd):
    """Encode a password so it can be used in an SQLAlchemy URL.

    Parameters
    ----------
    pwd : `str`
        The password to encode.

    Returns
    -------
    encoded_pwd : `str`
        The encoded password.

    Notes
    -----
    See the SQLAlchemy
    `docs <https://docs.sqlalchemy.org/en/14/core/engines.html#database-urls>`_
    for more info.
    """
    return urllib.parse.quote_plus(pwd)
