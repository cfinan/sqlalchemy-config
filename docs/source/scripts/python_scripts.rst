==============
Python Scripts
==============

sqlalchemy-config implements several command line scripts.

``orm-create-src``
------------------

.. _orm_create:

.. argparse::
   :module: sqlalchemy_config.orm_code
   :func: _init_cmd_args
   :prog: orm-create-src

Example usage
~~~~~~~~~~~~~

To create ORM Python code (i.e. full class definitions) from the reflection of the database connection supplied on the command line. The ORM Python code is output to ``orm_module.py``.

.. code-block:: console

    $ orm-create-src --reflect -o ~/orm_module.py sqlite:////my_database.db

As above but the database connection parameters are supplied in a configuration file ``~/.db.cnf`` under the section ``my_database_section``. In addition, a module path is supplied which is used in the docstrings for relationship attributes in ``orm_module.py``.

.. code-block:: console

    $ orm-create-src --config ~/.db.cnf --reflect -vv -o ~/orm_module.py --module-name "my_package.orm_module" my_database_section

As above but with additional data and documentation supplied in both the table and column information files. These allow the user to modify some of the reflected data and add detailed descriptions to columns.

.. code-block:: console

    $ orm-create-src  --column-info /data/my_database_column_info.txt --table-info /data/my_database_table_info.txt --config ~/.db.cnf --reflect -vv -o ~/orm_module.py --module-name "my_package.orm_module" my_database_section

Create ORM code file called ``clinical_trials_orm.py`` from a set of pipe (``|``) delimited text files located in the directory ``20230823``. Use full verbosity and use additional information supplied in the table-info file ``ctg_aact_table_info.txt`` and the column-info file ``ctg_aact_column_info.txt``. Finally, add 10% to all the ``VARCHAR`` column lengths and ensure that they are all rounded to the nearest multiple of 20.

.. code-block:: console

    $ orm-create-src --str-fudge-factor 0.1 --str-round-to 20 --table-info ctg_aact_table_info.txt --column-info ctg_aact_column_info.txt -d'|' -vv -o clinical_trials_orm.py --module-name "clinical_trials.orm" 20230823/*.txt
   === sqlalchemy-orm-code (sqlalchemy_config v0.1.1a0) ===
   [info] 08:55:58 > column_info value: ctg_aact_column_info.txt
   [info] 08:55:58 > comment value: #
   [info] 08:55:58 > config value: /home/rmjdcfi/.db.cnf
   [info] 08:55:58 > delimiter value: |
   [info] 08:55:58 > encoding value: UTF8
   [info] 08:55:58 > infiles length: 47
   [info] 08:55:58 > module_name value: clinical_trials.orm
   [info] 08:55:58 > outfile value: clinical_trials_orm.py
   [info] 08:55:58 > reflect value: False
   [info] 08:55:58 > sample value: -1
   [info] 08:55:58 > skip_lines value: 0
   [info] 08:55:58 > str_fudge_factor value: 0.1
   [info] 08:55:58 > str_text_cut value: 20000
   [info] 08:55:58 > str_round_to value: 20
   [info] 08:55:58 > table_info value: ctg_aact_table_info.txt
   [info] 08:55:58 > template_prefix value: None
   [info] 08:55:58 > verbose value: 2
   [info] processing files...: 100%|----------| 47/47 [24:05<00:00, 30.76s/ table/file(s)]
   [info] 09:20:04 > *** END ***

Input files
~~~~~~~~~~~

In addition to the database connection input or flat files to analyse, this script can accept a file describing the table information and column information. These are described below.

Table info file
...............

.. include:: ../data_dict/table-info.rst

Column info file
................

.. include:: ../data_dict/column-info.rst

Output file
~~~~~~~~~~~

This outputs PEP8 formatted Python source code for the ORM classes that represent the database connection, flat files or table/column info files given to the script.

Known Issues
~~~~~~~~~~~~

Currently for Indexed TEXT fields in databases such as MySQL, they will not be implemented correctly as they require a custom index length to be implemented. I will have a look at adding this in as soon as I get time.

``orm-test-connect``
--------------------

.. _orm_test:

.. argparse::
   :module: sqlalchemy_config.test_connect
   :func: _init_cmd_args
   :prog: orm-test-connect

Example usage
~~~~~~~~~~~~~

To test an ORM module ``orm.py`` against a database connection that is stored in a config file under the heading ``my_db``.
.. code-block::

    orm-test-connect -vv --config ~/.db.cnf my_db ~/code/my-orm-package/my_orm/orm.py

As above but testing a module that is actually installed on the system.

.. code-block::

    orm-test-connect -vv --config ~/.db.cnf my_db "my_orm.orm"

Known Issues
~~~~~~~~~~~~

Note that this does not directly test the column data types are matching between the ORM definitions and the database connection.

``orm-column-info``
-------------------

.. _col_info:

.. argparse::
   :module: sqlalchemy_config.column_info
   :func: _init_cmd_args
   :prog: orm-column-info

Example usage
~~~~~~~~~~~~~

This outputs the column information for a ``|`` (pipe) delimited file called ``interventions.txt``. The script is run with full verbosity and outputs to STDOUT, which is redirected to a file called ``interventions_info.txt``.

.. code-block::

   $ orm-column-info -d'|' -vv interventions.txt > interventions_info.txt
   === orm-column-info (sqlalchemy_config v0.1.1a0) ===
   [info] 11:58:16 > comment value: #
   [info] 11:58:16 > delimiter value: |
   [info] 11:58:16 > encoding value: UTF8
   [info] 11:58:16 > header_columns value: None
   [info] 11:58:16 > infile value: interventions.txt
   [info] 11:58:16 > no_header value: False
   [info] 11:58:16 > outfile value: None
   [info] 11:58:16 > skip_lines value: 0
   [info] 11:58:16 > verbose value: 2
   [info] 11:58:28 > *** END ***

Output file
~~~~~~~~~~~

This outputs either to STDOUT or to a file. The columns of the output are tab delimited and defined below:

.. include:: ../data_dict/orm-column-info.rst

Known Issues
~~~~~~~~~~~~

None reported.

``orm-doc-schema``
------------------

.. _orm_doc_schema:

.. argparse::
   :module: sqlalchemy_config.orm_doc
   :func: _init_orm_schema_cmd_args
   :prog: orm-doc-schema

Known Issues
~~~~~~~~~~~~

None reported.

``orm-docstring``
-----------------

.. _orm_docstring:

.. argparse::
   :module: sqlalchemy_config.orm_doc
   :func: _init_orm_doc_cmd_args
   :prog: orm-docstring

Known Issues
~~~~~~~~~~~~

None reported.

``orm-doc-api``
---------------

.. _orm_doc_api:

.. argparse::
   :module: sqlalchemy_config.orm_doc
   :func: _init_orm_api_doc_cmd_args
   :prog: orm-doc-api

Known Issues
~~~~~~~~~~~~

None reported.
