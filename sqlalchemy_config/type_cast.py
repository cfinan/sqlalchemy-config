"""A selection of small casting functions, that are useful to convert data that
has been read in from a text file.
"""
from datetime import datetime


BOOL_MAP = {
    'yes': True,
    'no': False,
    'y': True,
    'n': False,
    'true': True,
    'false': False,
    't': True,
    'f': False,
    '1': True,
    '0': False,
    1: True,
    0: False,
    True: True,
    False: False
}
"""A mapper between potential Boolean text values and Python Boolean types,
these assume that you know the data is Boolean (`dict`)
"""

BOOL_MAP_NONE = {**BOOL_MAP, **{None: None, '': None}}
"""A mapper between potential Boolean text values and Python Boolean types,
these assume that you know the data is Boolean or NoneType (`dict`)
"""
MISSING_VALUES = ['', None]
"""Values that indicate that a variable is undefined (`list`)
"""
DATES = [
    "%Y-%m-%d",
    "%B %d, %Y",
    "%B %Y",
    "%Y-%m-%d %H:%M:%s.%f",
    "%d/%m/%Y"
]
"""A selection of date parsing strings to cover many common use cases
(`list` of `str`).
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_bool(x):
    """Parse a value that should be text or numeric Boolean.

    Parameters
    ----------
    x : `str` or `int`
        The value to convert.

    Returns
    -------
    bool_x : `bool`
        The Boolean version of x.

    Raises
    ------
    KeyError
        If x is not in the Boolean map.
    """
    try:
        return BOOL_MAP[x.lower()]
    except KeyError as e:
        raise TypeError(f"unknown bool value: {x}") from e


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_bool_none(x):
    """Parse a value that should be text or numeric Boolean or NoneType.

    Parameters
    ----------
    x : `str` or `int`
        The value to convert.

    Returns
    -------
    bool_x : `bool` or `NoneType`
        The Boolean version of x.

    Raises
    ------
    KeyError
        If x is not in the Boolean map.
    """
    try:
        return BOOL_MAP_NONE[x.lower()]
    except KeyError as e:
        raise TypeError(f"unknown bool value: {x}") from e


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_int(x):
    """Parse a value that should be an integer.

    Parameters
    ----------
    x : `str` or `int`
        The value to convert.

    Returns
    -------
    int_x : `int`
        The integer version of x.
    """
    return int(float(x))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_int_none(x):
    """Parse a value that should be an integer or NoneType.

    Parameters
    ----------
    x : `str` or `int`
        The value to convert.

    Returns
    -------
    int_x : `int` or `NoneType`
        The integer version of x or undefined.
    """
    try:
        return int(float(x))
    except (ValueError, TypeError):
        if x in MISSING_VALUES:
            return None
        raise


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_str_not_none(x):
    """Parse a value that should be text and can't be NoneType.

    Parameters
    ----------
    x : `str`
        The value to convert.

    Returns
    -------
    str_x : `str`
        The string version of x.

    Raises
    ------
    TypeError
        If x is NoneType.
    """
    if x is None:
        raise TypeError("string can't be NoneType")
    return str(x)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_str_none(x):
    """Parse a value that should be text or undefined.

    Parameters
    ----------
    x : `str`
        The value to convert.

    Returns
    -------
    str_x : `str` or `NoneType`
        The string version of x.
    """
    if x is None:
        return x
    return str(x)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_float_none(x):
    """Parse a value that should be a float or NoneType.

    Parameters
    ----------
    x : `str` or `float`
        The value to convert.

    Returns
    -------
    float_x : `float`
        The float version of x.
    """
    try:
        return float(x)
    except TypeError:
        if x is None:
            return x
        raise
    except ValueError:
        if x == '':
            return None


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_date(x):
    """Parse a value that should be convertible into a date.

    Parameters
    ----------
    x : `str`
        The value to covert.

    Returns
    -------
    date_x : `datetime.datetime`
        The datetime version of x.

    Raises
    ------
    ValueError
        If x is not one of the recognised date formats.
    """
    for i in DATES:
        try:
            return datetime.strptime(i)
        except ValueError:
            pass
    raise ValueError(f"unknown date format: {x}")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_date_none(x):
    """Parse a value that should be convertible into a date or NoneType.

    Parameters
    ----------
    x : `str`
        The value to covert.

    Returns
    -------
    date_x : `datetime.datetime` or `NoneType`
        The datetime version of x.

    Raises
    ------
    ValueError
        If x is not one of the recognised date formats.
    """
    for i in DATES:
        try:
            return datetime.strptime(i)
        except ValueError:
            pass

    if x in MISSING_VALUES:
        return None
    raise ValueError(f"unknown date format: {x}")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_date_parser(date_patterns, allow_none=False):
    """Get a date parsing function that will convert specific date patterns.

    Parameters
    ----------
    date_patterns : `list` of `str`
        Date patterns to apply to the conversion.
    allow_none : `bool`, optional, default: `False`
        Should NoneType values be tolerated.

    Returns
    -------
    convert_func : `function`
        A date conversion function that accepts a single string parameter.
    """
    if allow_none is False:
        def _date_parser(x):
            for i in date_patterns:
                try:
                    return datetime.strptime(x, i)
                except ValueError:
                    pass

            raise ValueError(f"unknown date format: {x}")
        return _date_parser

    def _date_parser_none(x):
        for i in date_patterns:
            try:
                return datetime.strptime(x, i)
            except ValueError:
                pass

        if x in MISSING_VALUES:
            return None
        raise ValueError(f"unknown date format: {x}")
    return _date_parser_none

