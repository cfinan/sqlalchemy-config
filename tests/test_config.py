"""Test that the column info analysis returns the correct data types.
"""
from pyaddons import utils
from sqlalchemy_config import config
from sqlalchemy_utils import create_database
from sqlalchemy.orm import sessionmaker
import pytest
import os
import stat


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _create_config(config_dir, db_url, section_name="test-db",
                   key_prefix="mydb.", perms=stat.S_IRUSR | stat.S_IWUSR):
    """Create a config file with the database url within it
    """
    config_file = utils.get_temp_file(
        dir=config_dir, prefix="pytest_config", suffix=".ini"
    )

    with open(config_file, 'wt') as outfh:
        outfh.write(f"[{section_name}]\n")
        outfh.write(f"{key_prefix}url = {db_url}\n")

    os.chmod(config_file, perms)
    return config_file


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    'perms,error',
    (
        (stat.S_IRUSR | stat.S_IWUSR, False),
        (stat.S_IRGRP | stat.S_IWUSR, True),
    )
)
def test_check_config_permissions(tmpdir, perms, error):
    """Test get new sessionmaker with a database URL when the database does
    not exist and is not expected to exist.
    """
    config_file = _create_config(
        tmpdir, 'sqlite:////any.db', perms=perms
    )
    try:
        if error is False:
            config.check_config_permissions(config_file)
        else:
            with pytest.raises(PermissionError) as err:
                config.check_config_permissions(config_file)
                assert err.match(
                    "Ensure config file has correct permissions"
                ), "Wrong error"
    finally:
        # Remove config file
        os.unlink(config_file)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    'remove', (True, False)
)
def test_get_new_sm_config_not_exist(tmpdir, remove):
    """Test get new sessionmaker with a database URL when the database does
    not exist and is not expected to exist.
    """
    tempdb = utils.get_temp_file(dir=tmpdir, prefix="pytest_db", suffix=".db")
    url = f'sqlite:///{tempdb}'
    os.unlink(tempdb)

    if remove is False:
        create_database(url)

    section_name = "test-db"
    key_prefix = "mydb."
    config_file = _create_config(
        tmpdir, url, section_name=section_name, key_prefix=key_prefix
    )

    try:
        sm = config.get_new_sessionmaker(
            section_name, exists=False, config_arg=config_file,
            conn_prefix=key_prefix
        )
        assert isinstance(sm, sessionmaker), "not a sessionmaker"
    finally:
        if remove is False:
            os.unlink(tempdb)
        os.unlink(config_file)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_get_new_sm_config_exist(tmpdir):
    """Test get new sessionmaker with a database URL when the database does
    not exist and is not expected to exist.
    """
    tempdb = utils.get_temp_file(dir=tmpdir, prefix="pytest_db", suffix=".db")
    os.unlink(tempdb)
    url = f'sqlite:///{tempdb}'
    create_database(url)

    section_name = "test-db"
    key_prefix = "mydb."
    config_file = _create_config(
        tmpdir, url, section_name=section_name, key_prefix=key_prefix
    )

    try:
        sm = config.get_new_sessionmaker(
            section_name, exists=True, config_arg=config_file,
            conn_prefix=key_prefix
        )
        assert isinstance(sm, sessionmaker), "not a sessionmaker"
    finally:
        os.unlink(tempdb)
        os.unlink(config_file)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_get_new_sm_config_error_exist(tmpdir):
    """Test get new sessionmaker with a database URL when the database does
    not exist and is not expected to exist.
    """
    tempdb = utils.get_temp_file(dir=tmpdir, prefix="pytest_db", suffix=".db")
    os.unlink(tempdb)
    url = f'sqlite:///{tempdb}'

    section_name = "test-db"
    key_prefix = "mydb."
    config_file = _create_config(
        tmpdir, url, section_name=section_name, key_prefix=key_prefix
    )

    with pytest.raises(FileNotFoundError) as err:
        config.get_new_sessionmaker(
            section_name, exists=True, config_arg=config_file,
            conn_prefix=key_prefix
        )
    assert err.value.args[0].startswith(
        'Database is expected to exist but does not exist'
    ), "wrong error raised"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    'remove', (True, False)
)
def test_get_new_sm_not_exist(tmpdir, remove):
    """Test get new sessionmaker with a database URL when the database does
    not exist and is not expected to exist.
    """
    tempdb = utils.get_temp_file(dir=tmpdir, prefix="pytest_db", suffix=".db")
    url = f'sqlite:///{tempdb}'
    os.unlink(tempdb)

    if remove is False:
        create_database(url)

    try:
        sm = config.get_new_sessionmaker(url, exists=False)
        assert isinstance(sm, sessionmaker), "not a sessionmaker"
    finally:
        if remove is False:
            os.unlink(tempdb)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_get_new_sm_exist(tmpdir):
    """Test get new sessionmaker with a database URL when the database does
    not exist and is not expected to exist.
    """
    tempdb = utils.get_temp_file(dir=tmpdir, prefix="pytest_db", suffix=".db")
    os.unlink(tempdb)
    url = f'sqlite:///{tempdb}'
    create_database(url)
    try:
        sm = config.get_new_sessionmaker(url, exists=True)
        assert isinstance(sm, sessionmaker), "not a sessionmaker"
    finally:
        os.unlink(tempdb)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_get_new_sm_error_exist(tmpdir):
    """Test get new sessionmaker with a database URL when the database does
    not exist and is not expected to exist.
    """
    tempdb = utils.get_temp_file(dir=tmpdir, prefix="pytest_db", suffix=".db")
    os.unlink(tempdb)
    url = f'sqlite:///{tempdb}'

    with pytest.raises(FileNotFoundError) as err:
        config.get_new_sessionmaker(url, exists=True)
    assert err.value.args[0].startswith(
        'Database is expected to exist but does not exist'
    ), "wrong error raised"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_get_new_sm_not_error_exist(tmpdir):
    """Test get new sessionmaker with a database URL when the database does
    not exist and is not expected to exist.
    """
    tempdb = utils.get_temp_file(dir=tmpdir, prefix="pytest_db", suffix=".db")
    os.unlink(tempdb)
    url = f'sqlite:///{tempdb}'
    create_database(url)
    try:
        with pytest.raises(FileExistsError) as err:
            config.get_new_sessionmaker(
                url, exists=False, raise_on_exists=True
            )
        assert err.value.args[0].startswith(
            'Database exists but is not expected to exist'
        ), "wrong error raised"
    finally:
        os.unlink(tempdb)


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# @pytest.mark.parametrize(
#     'has_header,test_file,exp_results',
#     (
#         (
#             False,
#             (
#                 [0, "A", 2, 9, 0.34],
#                 [1, "B", 1, 8, 1],
#                 [10, "C", None, 7, 2.45],
#                 [None, "AA", 2, 6, 0.1],
#                 [None, "BBBB", 1, 5, 5E-04],
#                 [33, "CCC", 2, 4, 10001.234],
#             ),
#             (
#                 (C.Dtype.INTEGER, True, True, False, 2, 6, 4),
#                 (C.Dtype.VARCHAR, False, False, False, 4, 6, 6),
#                 (C.Dtype.BOOLEAN, True, False, False, 1, 6, 5),
#                 (C.Dtype.INTEGER, False, False, True, 1, 6, 6),
#                 (C.Dtype.FLOAT, False, False, False, 9, 6, 6),
#             ),
#         ),
#     )
# )
# def test_file_info(tmpdir, has_header, test_file, exp_results):
#     """Test that the file info returns the correct data types.
#     """
#     outfile = _write_data(
#         utils.get_temp_file(dir=str(tmpdir)), test_file, header=has_header
#     )

#     try:
#         fi = FileInfo(outfile, no_header=not has_header)
#         fi.set_dtypes()
#         for r, c in zip(exp_results, fi.info.columns):
#             assert r[0] == c.dtype, "wrong data type"
#             assert r[1] == c.nullable, "wrong nullable"
#             assert r[2] == c.ascending, "wrong ascending"
#             assert r[3] == c.descending, "wrong descending"
#             assert r[4] == c.max_len, "wrong max_len"
#             assert r[5] == c.ntests, "wrong ntests"
#             assert r[6] == c.nvalued_tests, "wrong nvalued_tests"
#     finally:
#         os.unlink(outfile)
