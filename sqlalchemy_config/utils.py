"""Some general useful SQLAlchemy functions. These are the sorts of things that
will be called from multiple scripts.
"""
try:
    # SQLAlchemy <= 1.3
    from sqlalchemy.ext.declarative.api import DeclarativeMeta
except ImportError:
    # SQLAlchemy >= 1.4
    from sqlalchemy.orm import DeclarativeMeta
from sqlalchemy.engine import Engine
from sqlalchemy import event


DUCKDB_TYPE = 'duckdb'
"""The type string for a duckdb database (`str`)
"""
SQLITE_TYPE = 'sqlite'
"""The type string for a sqlite database (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    """Setup SQLite foreign keys upon an SQLAlchemy connection. Note this does
    not have to be called as it is called via the event listener.
    """
    cursor = dbapi_connection.cursor()
    try:
        cursor.execute("PRAGMA foreign_keys=ON")
    except Exception:
        # Probably not SQLite but do not know how to tell!
        pass
    cursor.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_orm_mappers(orm_module):
    """Return all the ORM mapper classes that are within the ORM module.

    Parameters
    ----------
    orm_module : `module`
        The ORM module to analyse.

    Returns
    -------
    orm_mappers : `list` or `sqlalchemy.ext.declarative.Base`
        Mapper objects that inherit from sqlalchemy.ext.declarative.Base.
    """
    orm_mappers = dict()
    for i in dir(orm_module):
        if not i.startswith('__'):
            obj = getattr(orm_module, i)

            if isinstance(obj, DeclarativeMeta):
                columns = dict()
                try:
                    for c in obj.__table__.columns:
                        columns[c.name] = c
                    orm_mappers[obj.__table__.name] = obj
                except AttributeError:
                    pass
    return orm_mappers


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def print_orm_obj(obj, exclude=[]):
    """Prints the contents of the SQL Alchemy ORM object.

    Parameters
    ----------
    obj : `sqlalchemy.ext.declarative.Base`
        The SQLAlchemy declarative base model object. This should have a
        __table__ attribute that contains the corresponding SQLAlchemy table
        object.
    exclude : `NoneType` or `list` or `str`, optional, default `NoneType`
        A list of attributes (these correspond to table columns) that you do
        not want to include in the string.

    Returns
    -------
    formatted_string : `str`
        A formatted string that can be printed. The string contains the
        attributes of the object and the data they contain.

    Notes
    -----
    This is designed to be called from an SQLAlchemy ORM model object and it
    in the __repr__ method. It simply frovides a formatted string that has the
    data in the object.
    """
    exclude = exclude or []

    attr_values = []
    for i in obj.__table__.columns:
        if i.name in exclude:
            continue
        val = getattr(obj, i.name)
        if val.__class__.__name__ != "method":
            attr_values.append("{0}={1}".format(i.name, val))

    # Turn outlist into a string and return
    return "<{0}({1})>".format(obj.__class__.__name__, ", ".join(attr_values))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_tables(session, orm_module):
    """Ensure all the tables are created.

    Parameters
    ----------
    session : `sqlalchemy.session`
        The session object, must return the engine when `get_bind()` is called
        on it.
    orm_module : `module`
        The ORM module that implements a Base object i.e. ``orm_module.Base``
        is accessible.
    """
    orm_module.Base.metadata.create_all(
        session.get_bind(), checkfirst=True
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def file_db_type(indb):
    """Determine the type (format) of a file based database. This can be either
    ``sqlite`` or ``duckdb``.

    Parameters
    ----------
    indb : `str`
        The path to the input database.

    Returns
    -------
    dbtype : `str`
        The database type either ``duckdb`` or ``sqlite``.

    Raises
    ------
    TypeError
        If the database type can't be determined.
    """
    if is_duckdb(indb) is True:
        return DUCKDB_TYPE
    elif is_sqlite(indb) is True:
        return SQLITE_TYPE
    else:
        raise TypeError(f"unknown file type: {indb}")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def is_duckdb(indb):
    """Determine if the database file has the magic bytes consistant with being
    a duckdb format.

    Parameters
    ----------
    indb : `str`
        The path to the input database.

    Returns
    -------
    is_duckdb : `bool`
        A flag indicating if the database file resembles a duckdb database.
    """
    with open(indb, 'rb') as db:
        test_bytes = db.read(13)
        return test_bytes[8:12] == b'DUCK'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def is_sqlite(indb):
    """Determine if the database file has the magic bytes consistant with being
    a SQLite3 format.

    Parameters
    ----------
    indb : `str`
        The path to the input database.

    Returns
    -------
    is_sqlite : `bool`
        A flag indicating if the database file resembles an SQLite3 database.
    """
    with open(indb, 'rb') as db:
        test_bytes = db.read(16)
        return test_bytes == b'SQLite format 3\x00'
