"""A utility for writing ORM code from either a database connection or a
a set of flat files that represent database tables. This is intended to write
python ORM code as a template that will then be adapted by the user.

The user can also provide additional information about the tables and the
 columns in the form of info files.

The code it generates may not be 100% correct but is a best guess. Having said
that it will handle things like setting up relationships for foreign key
columns. It can also handle multiple foreign keys referencing a single
table/column.
"""
# Importing the version number (in __init__.py) and the package name
from sqlalchemy_config import (
    __version__,
    __name__ as pkg_name,
    config as cfg,
    type_cast as tc
)
from pyaddons import (
    log,
    utils
)
from sqlalchemy.ext.declarative import declarative_base
from pyaddons.flat_files import header
from tqdm import tqdm
from enum import Enum
from datetime import datetime
import textwrap
import argparse
import sys
import os
import gzip
import re
import pathlib
import csv
import stdopen
import warnings
import sqlalchemy
import math
# import pprint as pp


_SCRIPT_NAME = "sqlalchemy-orm-code"
"""The name of the script (`str`)
"""
_DESC = __doc__
"""The program description (`str`)
"""
try:
    _DEFAULT_CONFIG = os.path.join(os.environ['HOME'], '.db.cnf')
    """The default fallback path for the config file, root of the home
    directory (`str`)
    """
except KeyError:
    # HOME environment variable does not exist for some reason
    _DEFAULT_CONFIG = None

_DEFAULT_SECTION = 'db'
"""The default section name in an uni config file that contains SQLAlchemy
connection parameters (`str`)
"""
_DEFAULT_PREFIX = 'db.'
"""The default prefix name in an ini config section each SQLAlchemy connection
parameter should be prefixed with this (`str`)
"""
_DELIMITER = "\t"
"""The default delimiter for the ``table_info`` and ``column_info`` files
(`str`).
"""

# Constants for column names in the table/column info files
TABLE_NAME_COL = 'table_name'
"""The name for the table name column (`str`)
"""
TABLE_NAME_NEW_COL = 'new_table_name'
"""The name for the new table name column (`str`)
"""
TABLE_CLASS_NAME_COL = 'class_name'
"""The name for the table class column (`str`)
"""
TABLE_DOC_COL = 'table_doc'
"""The name for the table description column (`str`)
"""
COLUMN_NAME_COL = 'column_name'
"""The name for the column name column (`str`)
"""
COLUMN_NAME_NEW_COL = 'new_column_name'
"""The name for the new column name column (`str`)
"""
COLUMN_DTYPE = 'dtype'
"""The name for the column data type column (`str`)
"""
COLUMN_MAX_LEN = 'max_len'
"""The name for the column maximum field length column (`str`)
"""
COLUMN_PRIMARY_KEY = 'is_primary_key'
"""The name for the column is a primary key field (`str`)
"""
COLUMN_INDEX = 'is_index'
"""The name for the column is an index field (`str`)
"""
COLUMN_NULLABLE = 'is_nullable'
"""The name for the column is nullable field (`str`)
"""
COLUMN_UNIQUE = 'is_unique'
"""The name for the column is unique field (`str`)
"""
COLUMN_FOREIGN_KEY = 'foreign_key'
"""The name for the column foreign key field (`str`)
"""
COLUMN_DOC = 'column_doc'
"""The name for the column description field (`str`)
"""

# Code indentation levels
_INDENT_CHAR = " "
"""The code indent character `str`
"""
_INDENT_LEN = 4
"""The code indent length `int`
"""
_LEVEL = 0
"""The root code indent level `int`
"""
_LINE_WIDTH = 79
"""The max length of each code line `int`
"""
ONE_INDENT = (_INDENT_LEN * _INDENT_CHAR)
"""The code indent for 1 level indented code `str`
"""
TWO_INDENT = ((2 * _INDENT_LEN) * _INDENT_CHAR)
"""The code indent for 2 level indented code `str`
"""
THREE_INDENT = ((3 * _INDENT_LEN) * _INDENT_CHAR)
"""The code indent for 3 level indented code `str`
"""
FOUR_INDENT = ((4 * _INDENT_LEN) * _INDENT_CHAR)
"""The code indent for 4 level indented code `str`
"""
FIVE_INDENT = ((5 * _INDENT_LEN) * _INDENT_CHAR)
"""The code indent for 5 level indented code `str`
"""

_RELATIONSHIP_CLASS = 'relationship'
"""The SQLAlchemy relationship class, this may get aliased if the name
 collides with a database column name (`str`)
"""
COLUMN_CLASS = 'Column'
"""The SQLAlchemy column class, this may get aliased if the name
 collides with a database column name (`str`)
"""

_PRINT_FUNC = '''
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def print_orm_obj(obj, exclude=[]):
    """Prints the contents of the SQL Alchemy ORM object.

    Parameters
    ----------
    obj : `sqlalchemy.ext.declarative.Base`
        The SQLAlchemy declarative base model object. This should have a
        __table__ attribute that contains the corresponding SQLAlchemy table
        object.
    exclude : `NoneType` or `list` or `str`, optional, default `NoneType`
        A list of attributes (these correspond to table columns) that you do
        not want to include in the string.

    Returns
    -------
    formatted_string : `str`
        A formatted string that can be printed. The string contains the
        attributes of the object and the data they contain.

    Notes
    -----
    This is designed to be called from an SQLAlchemy ORM model object and it
    in the __repr__ method. It simply frovides a formatted string that has the
    data in the object.
    """
    exclude = exclude or []

    attr_values = []
    for i in obj.__table__.columns:
        if i.name in exclude:
            continue
        val = getattr(obj, i.name)
        if val.__class__.__name__ != "method":
            attr_values.append("{0}={1}".format(i.name, val))

    # Turn outlist into a string and return
    return "<{0}({1})>".format(obj.__class__.__name__, ", ".join(attr_values))
'''
"""Template function code for ORM object printing (`str`)
"""
_REPR_METHOD = '''
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)
'''
"""Template function code for ORM object printing (`str`)
"""

# Make sure csv can deal with the longest lines possible
csv.field_size_limit(sys.maxsize)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _BaseColumn(object):
    """A representation of a column.

    Parameters
    ----------
    column_representation : `any`
        The representation of the column This might be a data source to query
        or simply a column name.
    name : `str`, optional, default: `NoneType`
        A column name if not represented by the column representation.
    dtype : `str`
        The column data type. These should be one of the
        `sqlalchemy_config.orm_code.Column.Dtype` enumerations.
    nullable : `bool`, optional, default: `False`
        Can the column contain ``NoneType`` values.
    max_len : `int`, optional, default: `0`
        If the dtype is a string then what is the max value.
    index : `bool`, optional, default: `False`
        Should the column be indexed.
    null_values : `list` of `str` or `NoneType`, optional,
    default: `[NoneType, '']`
        Values that are considered to be empty values in the test data.
        ints/floats are not supported here.
    doc : `str`, optional, default: `NoneType`
        A column description, this will be placed in the SQLAlchemy column
        `doc` attribute.
    primary_key : `bool`, optional, default: `False`
        Is the column a primary key column?
    foreign_keys : `list` of (`tuple` or `list`), optional, default: `NoneType`
        Any foreign key references, eahc foreign key should be of length 2 with
        [0] being the reference table name and `[1]` being the reference column
        name.
    default_value : `any`, optional, default: `NoneType`
        The default value for a column if it is not `NoneType`
    str_fudge_factor : `float`, optional, default: `0`
        Increase the string lengths by this proportion.
    str_round_to : `int`, optional, default: `0`
        Round string lengths to the nearest ``str_round_to``.
    str_text_cut : `int`, optional, default: `20000`
        The length in characters that VARCHARs are converted to TEXT fields.
    """

    # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    class Dtype(Enum):
        """An enumeration for a data type that has been detected.
        """
        INTEGER = 'int'
        NUMERIC = 'numeric'
        BOOLEAN = 'bool'
        BIG_INTEGER = 'big_int'
        SMALL_INTEGER = 'small_int'
        TEXT = 'text'
        VARCHAR = 'varchar'
        DATE = 'date'
        DATE_TIME = 'datetime'
        FLOAT = 'float'
        URL = 'url'

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        @classmethod
        def get_sqlalchemy_class(cls, dtype, **kwargs):
            """Get the python code text for the SQLAlchemy class name for a
            data type.

            Parameters
            ----------
            dtype : `str` or `Dtype`
                The enumerated data type.
            **kwargs
                Any other keyword arguments, currently only max_len is checked
                 for VARCHAR/String types.
            """
            if dtype == cls.INTEGER or cls(dtype) == cls.INTEGER:
                return "Integer"
            elif dtype == cls.NUMERIC or cls(dtype) == cls.NUMERIC:
                return "Numeric"
            elif dtype == cls.BOOLEAN or cls(dtype) == cls.BOOLEAN:
                return "Boolean"
            elif dtype == cls.BIG_INTEGER or cls(dtype) == cls.BIG_INTEGER:
                return "BigInteger"
            elif dtype == cls.SMALL_INTEGER or cls(dtype) == cls.SMALL_INTEGER:
                return "SmallInteger"
            elif dtype == cls.TEXT or cls(dtype) == cls.TEXT:
                return "Text"
            elif dtype == cls.VARCHAR or cls(dtype) == cls.VARCHAR:
                length = kwargs.pop("max_len", 255)

                # Make sure that zero length columns have something sensible
                if length == 0:
                    length = 255
                return f"String({length})"
            elif dtype == cls.DATE or cls(dtype) == cls.DATE:
                return "Date"
            elif dtype == cls.DATE_TIME or cls(dtype) == cls.DATE_TIME:
                return "DateTime"
            elif dtype == cls.URL or cls(dtype) == cls.URL:
                return "Text"
            elif dtype == cls.FLOAT or cls(dtype) == cls.FLOAT:
                return "Float"
            else:
                raise TypeError(f"Unknown data type: {dtype}")

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        @classmethod
        def get_python_class(cls, dtype):
            """Get the python code text for the SQLAlchemy class name for a
            data type.

            Parameters
            ----------
            dtype : `str` or `Dtype`
                The enumerated data type.
            **kwargs
                Any other keyword arguments, currently only max_len is checked
                 for VARCHAR/String types.
            """
            if dtype == cls.INTEGER or cls(dtype) == cls.INTEGER:
                return "int"
            elif dtype == cls.NUMERIC or cls(dtype) == cls.NUMERIC:
                return "int"
            elif dtype == cls.BOOLEAN or cls(dtype) == cls.BOOLEAN:
                return "bool"
            elif dtype == cls.BIG_INTEGER or cls(dtype) == cls.BIG_INTEGER:
                return "int"
            elif dtype == cls.SMALL_INTEGER or cls(dtype) == cls.SMALL_INTEGER:
                return "int"
            elif dtype == cls.TEXT or cls(dtype) == cls.TEXT:
                return "str"
            elif dtype == cls.VARCHAR or cls(dtype) == cls.VARCHAR:
                return "str"
            elif dtype == cls.DATE or cls(dtype) == cls.DATE:
                return "date.date"
            elif dtype == cls.DATE_TIME or cls(dtype) == cls.DATE_TIME:
                return "date.datetime"
            elif dtype == cls.URL or cls(dtype) == cls.URL:
                return "str"
            elif dtype == cls.FLOAT or cls(dtype) == cls.FLOAT:
                return "float"
            else:
                raise TypeError(f"Unknown data type: {dtype}")

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        @classmethod
        def get_python_cast(cls, dtype, **kwargs):
            """Get a python function to cast a variable to the correct type. In
            most cases it assumes that the variable will be cast from a string.

            Parameters
            ----------
            dtype : `str` or `Dtype`
                The enumerated data type.
            **kwargs
                Any other keyword arguments, currently only ``date_format``
                (`list` of `str`) and allow_none (`bool`) is supported.

            Raises
            ------
            TypeError
                If the ``dtype`` is unknown.
            """
            allow_none = kwargs.pop('allow_none', False)
            if dtype == cls.INTEGER or cls(dtype) == cls.INTEGER:
                if allow_none is False:
                    return tc.parse_int
                return tc.parse_int_none
            elif dtype == cls.NUMERIC or cls(dtype) == cls.NUMERIC:
                if allow_none is False:
                    return tc.parse_int
                return tc.parse_int_none
            elif dtype == cls.BOOLEAN or cls(dtype) == cls.BOOLEAN:
                if allow_none is False:
                    return tc.parse_bool
                return tc.parse_bool_none
            elif dtype == cls.BIG_INTEGER or cls(dtype) == cls.BIG_INTEGER:
                if allow_none is False:
                    return tc.parse_int
                return tc.parse_int_none
            elif dtype == cls.SMALL_INTEGER or cls(dtype) == cls.SMALL_INTEGER:
                if allow_none is False:
                    return tc.parse_int
                return tc.parse_int_none
            elif dtype == cls.TEXT or cls(dtype) == cls.TEXT:
                if allow_none is False:
                    return tc.parse_str_not_none
                return tc.parse_str_none
            elif dtype == cls.VARCHAR or cls(dtype) == cls.VARCHAR:
                if allow_none is False:
                    return tc.parse_str_not_none
                return tc.parse_str_none
            elif dtype == cls.DATE or cls(dtype) == cls.DATE:
                dates = kwargs.pop('date_format', tc.DATES)
                return tc.get_date_parser(dates, allow_none=allow_none)
            elif dtype == cls.DATE_TIME or cls(dtype) == cls.DATE_TIME:
                dates = kwargs.pop('date_format', tc.DATES)
                return tc.get_date_parser(dates, allow_none=allow_none)
            elif dtype == cls.URL or cls(dtype) == cls.URL:
                if allow_none is False:
                    return tc.parse_str_not_none
                return tc.parse_str_none
            elif dtype == cls.FLOAT or cls(dtype) == cls.FLOAT:
                if allow_none is False:
                    return float
                return tc.parse_float_none
            else:
                raise TypeError(f"Unknown data type: {dtype}")

    STRING_BOOLS = [
        re.compile('^[tf]$', re.IGNORECASE),
        re.compile('^yes|no$', re.IGNORECASE),
        re.compile('^y|n$', re.IGNORECASE),
        re.compile('^true|false$', re.IGNORECASE),
    ]
    """Regular expressions that indicate string boolean values, these will
    only be applied if the number of values observed is 2
    (`list` of `re.Pattern`)
    """
    STRING_DATES = [
        '%Y-%m-%d', '%d-%m-%Y', '%Y/%m/%d', '%d/%m/%Y', '%B %Y', '%B %-d,%Y'
    ]
    """Date parsing templates that indicate if string values are actually dates
    (`list` of `str`)
    """
    STRING_DATE_TIME = [
        '%Y-%m-%d %H:%M:%S.%f'
    ]
    """Date parsing templates that indicate if string values are actually
    datetimes (`list` of `str`)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, column_representation, name=None, dtype=Dtype.VARCHAR,
                 nullable=False, max_len=None, index=False, doc=None,
                 null_values=[None, ''], primary_key=False, unique=False,
                 foreign_keys=None, default_value=None, str_fudge_factor=0,
                 str_round_to=0, str_text_cut=20000):
        self._col_rep = column_representation
        self.name = name
        self._dtype = dtype
        self.nullable = nullable
        self.max_len = max_len or 0
        self.index = index
        self.null_values = null_values
        self.primary_key = primary_key
        self.unique = unique
        self.doc = doc
        self.default_value = default_value
        self.foreign_keys = foreign_keys or []
        self.str_fudge_factor = str_fudge_factor
        self.str_round_to = str_round_to
        self.str_text_cut = str_text_cut
        self._nvalues = set()

        # These are flags for testing if strings might be booleans or dates
        # once there is evidence that this is not the case they are set to
        # False
        self.test_str_bool = True
        self.test_str_date = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """Pretty printing.
        """
        atts = [
            f"name='{self.name}'",
            f"dtype='{self.dtype}'",
            f"nullable='{self.nullable}'",
            f"max_len='{self.max_len}'",
            f"index='{self.index}'",
            f"nvalues='{self.nvalues}'",
        ]
        return "<{0}({1})>".format(
            self.__class__.__name__, ",".join(atts)
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def sqlalchemy_class_str(self):
        """Get the name of the SQLAlchemy class for the column (`str`)
        """
        return re.sub(
            r'\(.+$',
            '',
            self.Dtype.get_sqlalchemy_class(
                self.dtype, max_len=self.max_len
            )
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def dtype(self):
        """Get the data type for the column (`str`)
        """
        if self._dtype == self.Dtype.INTEGER:
            # If we have a boolean data type then we make sure it is still
            # valid
            if len(self._nvalues) == 2:
                return self.Dtype.BOOLEAN
            elif self.max_len > 9:
                return self.Dtype.BIG_INTEGER
        elif self._dtype == self.Dtype.VARCHAR and \
             round_to_nearest(
                 self.max_len, fudge_factor=self.str_fudge_factor,
                 round_to=self.str_round_to
             ) > self.str_text_cut:
            # If we are text and have are over the text cut point then return
            # TEXT and not VARCHAR
            return self.Dtype.TEXT
        return self._dtype

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @dtype.setter
    def dtype(self, dtype):
        """Set the data type for the column (`str`)
        """
        self._dtype = dtype

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def orm_code(self, inset=4, width=79, level=0, column_class=COLUMN_CLASS):
        """Return the ORM code for the SQLAlchemy class.

        Parameters
        ----------
        inset : `int`, optional, default: `4`
            The number of spaces that is required for a single level
            indentation.
        width : `int`, optional, default: `79`
            The maximum code code width.
        level : `int`, optional, default: `0`
            The indentation level for the ORM code.
        column_class : `str`, optional, default: `Column`
            The column class name to use for the SQLAlchemy column definitions.

        Returns
        -------
        orm_code : `str`
            An ORM class definition.
        """
        one = (" " * inset)
        two = 2 * (" " * inset)
        args = [
            self.Dtype.get_sqlalchemy_class(
                self.dtype, max_len=round_to_nearest(
                    self.max_len, fudge_factor=self.str_fudge_factor,
                    round_to=self.str_round_to
                )
            )
        ]

        for i in self.foreign_keys:
            args.append(
                f'ForeignKey("{i[0]}.{i[1]}")'
            )
            self.index = True

        kwargs = [
            f"index={str(self.index)}",
            f"unique={str(self.unique)}",
            f"nullable={str(self.nullable)}",
        ]

        if self.primary_key is True:
            args.append(
                f'Sequence("{self.name}_seq")'
            )
            kwargs.append(
                f"primary_key={str(self.primary_key)}"
            )
            # Only use auto increment if the data type is integer based
            if self.dtype in \
               (self.Dtype.INTEGER, self.Dtype.BIG_INTEGER,
                self.Dtype.SMALL_INTEGER):
                kwargs.append(
                    'autoincrement=True'
                )

        if self.doc is not None and len(self.doc) > 0:
            kwargs.append(f'doc="{self.doc}"')

        return "{0} = {3}(\n{1}\n{2})".format(
            self.name, format_args(args, kwargs, indent=len(two), width=width),
            one, column_class
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def docstring_code(self, inset=4, width=79, level=0):
        """Return the formatted docstring code.

        Parameters
        ----------
        inset : `int`, optional, default: `4`
            The number of spaces that is required for a single level
            indentation.
        width : `int`, optional, default: `79`
            The maximum code code width.
        level : `int`, optional, default: `0`
            The indentation level for the ORM code.

        Returns
        -------
        column_docstring_code : `str`
            The column docstring code to go into the numpy docstring.
        """
        one = (" " * inset)
        two = 2 * (" " * inset)
        dtype = self.Dtype.get_python_class(self.dtype)
        base_param = f'{self.name} : `{dtype}`'

        if self.nullable is True:
            default_value = self.default_value
            if self.default_value is None:
                default_value = 'NoneType'

            base_param = f'{base_param}, optional, default: `{default_value}`'

        base_param = _wrap_docstring_dtype(
            base_param, width=width, indent_chars=one, return_str=False
        )

        doc = self.doc
        if doc is None or len(doc) == 0:
            doc = f"The ``{self.name}`` column."
        doc = self._update_doc(doc)

        doc = textwrap.wrap(
            doc, width=width, initial_indent=two, subsequent_indent=two
        )
        return "\n".join(base_param + doc)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _update_doc(self, doc):
        """Update any docstring parameter description.

        Returns
        -------
        doc : `str`
            The parameter description.
        """
        # if doc == "":
        #     doc = f"The ``{self.name}`` column."

        if not doc.endswith('.'):
            doc = doc + '.'

        if self.index is True:
            doc = f'{doc} This is indexed.'

        if self.unique is True:
            doc = f'{doc} This is unique.'

        if len(self.foreign_keys) > 0:
            fk = ", ".join(
                [f"``{t}.{c}``" for t, c in self.foreign_keys]
            )
            doc = f'{doc} Foreign key to {fk}.'

        if self.dtype == self.Dtype.VARCHAR or self.dtype == self.Dtype.TEXT:
            ml = self.max_len
            if ml == 0:
                ml = 255
            else:
                ml = round_to_nearest(
                    self.max_len, fudge_factor=self.str_fudge_factor,
                    round_to=self.str_round_to
                )
            doc = re.sub(r'\.$', '', doc)
            doc = f'{doc} (length {ml}).'
        return doc


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class FileColumn(_BaseColumn):
    """A representation of a column.

    Parameters
    ----------
    column_representation : `str`
        The column name.
    name : `str`, optional, default: `NoneType`
        A column name if not represented by the column representation.
    dtype : `str`
        The column data type. These should be one of the
        `sqlalchemy_config.orm_code.Column.Dtype` enumerations.
    nullable : `bool`, optional, default: `False`
        Can the column contain ``NoneType`` values.
    max_len : `int`, optional, default: `0`
        If the dtype is a string then what is the max value.
    index : `bool`, optional, default: `False`
        Should the column be indexed.
    null_values : `list` of `str` or `NoneType`, optional,
    default: `[NoneType, '']`
        Values that are considered to be empty values in the test data.
        ints/floats are not supported here.
    desc : `str`, optional, default: `NoneType`
        A column description, this will be placed in the SQLAlchemy column
        `doc` attribute.
    store_values : `int`, optional, default: `10`
        A number of unique data values to store for a column. This will be used
        to detect Boolean fields, i.e. ints that only two possible values, or
        strings that have yes/no values.
    primary_key : `bool`, optional, default: `False`
        Is the column a primary key column?
    foreign_keys : `list` of (`tuple` or `list`), optional, default: `NoneType`
        Any foreign key references, eahc foreign key should be of length 2 with
        [0] being the reference table name and `[1]` being the reference column
        name.
    default_value : `any`, optional, default: `NoneType`
        The default value for a column if it is not `NoneType`
    str_fudge_factor : `float`, optional, default: `0`
        Increase the string lengths by this proportion.
    str_round_to : `int`, optional, default: `0`
        Round string lengths to the nearest ``str_round_to``.
    str_text_cut : `int`, optional, default: `20000`
        The length in characters that VARCHARs are converted to TEXT fields.
    """
    FLOAT_REGEX = re.compile(r'^-?(?:0|[1-9]\d*)(?:\.\d+)(?:[eE][+\-]?\d+)?$')
    """A float regex (this will not match ints) (`re.Pattern`)
    """
    INT_REGEX = re.compile(r'^-?(?:0|[1-9]\d*)(?:[eE][+\-]?\d+)?$')
    """An integer regex (`re.Pattern`)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, column_representation, store_vals=10, **kwargs):
        kwargs['name'] = column_representation
        super().__init__(column_representation, **kwargs)
        self.name = column_representation
        self.store_vals = store_vals
        self._prev_value = None
        self._ascending = True
        self._descending = True
        self._tests = 0
        self._valued_tests = 0
        self.date_converters = set()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """Pretty printing.
        """
        atts = [
            f"name='{self.name}'",
            f"dtype='{self.dtype}'",
            f"nullable='{self.nullable}'",
            f"max_len='{self.max_len}'",
            f"index='{self.index}'",
            f"ascending='{self.ascending}'",
            f"descending='{self.descending}'",
            f"ntests='{self._tests}'",
            f"nvalued_tests='{self._valued_tests}'",
            f"nvalues='{self.nvalues}'",
        ]
        return "<{0}({1})>".format(
            self.__class__.__name__, ",".join(atts)
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def ascending(self):
        """An indicator flag if an integer column is sequential, this could
        indicate a primary key column (`bool`)
        """
        if self.dtype == self.dtype.INTEGER:
            return self._ascending
        return False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def descending(self):
        """An indicator flag if an integer column is sequential, this could
        indicate a primary key column (`bool`)
        """
        if self.dtype == self.dtype.INTEGER:
            return self._descending
        return False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def ntests(self):
        """An indicator flag if an integer column is sequential, this could
        indicate a primary key column (`bool`)
        """
        return self._tests

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def nvalued_tests(self):
        """An indicator flag if an integer column is sequential, this could
        indicate a primary key column (`bool`)
        """
        return self._valued_tests

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def nvalues(self):
        """Get the number of stored values for the column, stored values are
        used for Boolean detection (`int`).
        """
        return len(self._nvalues)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def dtype(self):
        """Get the data type for the column (`str`)
        """
        dtype = super().dtype

        # If everything is NULL
        if self._valued_tests == 0 and self.ntests > 0:
            return self.Dtype.VARCHAR
        return dtype

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @dtype.setter
    def dtype(self, dtype):
        """Set the data type for the column (`str`)
        """
        self._dtype = dtype

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_value(self, value):
        """Test a value against a column and update the column definiton if
        necessary.

        Parameters
        ----------
        value : `any`
            A value to test for a data type.
        """
        # Start testing the value
        self._tests += 1

        if value in self.null_values:
            self.nullable = True
            # self._prev_value = value
            return

        # If we get here we have a value
        self._valued_tests += 1

        # Take the length anyway, just in case is is move to
        # string later
        self.max_len = max(len(value), self.max_len)

        # Store the value if we have not reached the threshold limit
        if len(self._nvalues) < self.store_vals:
            self._nvalues.add(value)

        if self._dtype == self.Dtype.VARCHAR:
            # Already at the lowest level
            self._prev_value = value

            if self._test_dates(value) is True:
                return
            elif self._test_str_bool(value) is True:
                return
            return

        if self._dtype == self.Dtype.BOOLEAN and \
           self._test_str_bool(value) is True:
            self._prev_value = value
            return

        if self._dtype == self.Dtype.DATE and \
           self._test_dates(value) is True:
            self._prev_value = value
            return

        # Not a float, so we will call it in int
        if not self.FLOAT_REGEX.match(value):
            self._dtype = self.Dtype.INTEGER
        else:
            self._dtype = self.Dtype.FLOAT
            self._prev_value = value
            return

        # Not a int, so we will call it a string
        if not self.INT_REGEX.match(value):
            self._prev_value = value
            self._dtype = self.Dtype.VARCHAR
        else:
            try:
                pv = int(self._prev_value)

                if int(value) > pv:
                    self._ascending = self._ascending & True
                    self._descending = False
                elif int(value) < pv:
                    self._descending = self._descending & True
                    self._ascending = False
                else:
                    self._descending = False
                    self._ascending = False
            except (TypeError, ValueError):
                if self._prev_value is not None:
                    self._descending = False
                    self._ascending = False
        self._prev_value = value

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _test_dates(self, value):
        if self.test_str_date is True:
            for i in self.STRING_DATES:
                try:
                    datetime.strptime(value, i)
                    self._dtype = self.Dtype.DATE
                    # If we have a date match store the format string used to
                    # match
                    self.date_converters.add(i)
                    return True
                except ValueError:
                    pass
            self.test_str_date = False
        return False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _test_str_bool(self, value):
        if self.test_str_bool is True:
            for i in self.STRING_BOOLS:
                if i.match(value):
                    self._dtype = self.Dtype.BOOLEAN
                    return True
            self.test_str_bool = False
        return False


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class TableColumn(_BaseColumn):
    """A representation of a column.

    Parameters
    ----------
    column_representation : `sqlalchemy.Column`
        The column.
    str_fudge_factor : `float`, optional, default: `0`
        Increase the string lengths by this proportion.
    str_round_to : `int`, optional, default: `0`
        Round string lengths to the nearest ``str_round_to``.
    str_text_cut : `int`, optional, default: `20000`
        The length in characters that VARCHARs are converted to TEXT fields.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, column_representation, str_fudge_factor=0,
                 str_round_to=0, str_text_cut=20000):
        self.column = column_representation
        dtype, max_len = self._get_type()
        fk = []
        for i in self.column.foreign_keys:
            fk.append(i.target_fullname.split('.'))

        super().__init__(
            self.column.name, name=self.column.name, dtype=dtype,
            nullable=self.column.nullable or False, max_len=max_len,
            index=self.column.index or False, doc=self.column.doc,
            primary_key=self.column.primary_key, foreign_keys=fk,
            str_fudge_factor=str_fudge_factor, str_round_to=str_round_to,
            unique=self.column.unique or False, str_text_cut=str_text_cut
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_type(self):
        """Get the column data type from the SQLAlchemy column data type.

        Returns
        -------
        dtype : `str`
            The data type string
        max_len : `int`
            The max length of the strings in the column. Will be 0 if not a
            string column.
        """
        dtype = self.Dtype.VARCHAR
        max_len = 0

        if isinstance(self.column.type, sqlalchemy.String):
            dtype = self.Dtype.VARCHAR
            max_len = self.column.type.length
        elif isinstance(self.column.type, sqlalchemy.Text):
            dtype = self.Dtype.VARCHAR
            max_len = self.column.type.length
        elif isinstance(self.column.type, sqlalchemy.BigInteger):
            dtype = self.Dtype.BIG_INTEGER
        elif isinstance(self.column.type, sqlalchemy.SmallInteger):
            dtype = self.Dtype.SMALL_INTEGER
        elif isinstance(self.column.type, sqlalchemy.Integer):
            dtype = self.Dtype.INTEGER
        elif isinstance(self.column.type, sqlalchemy.Numeric):
            dtype = self.Dtype.FLOAT
        elif isinstance(self.column.type, sqlalchemy.Float):
            dtype = self.Dtype.FLOAT
        elif isinstance(self.column.type, sqlalchemy.Date):
            dtype = self.Dtype.DATE
        elif isinstance(self.column.type, sqlalchemy.DateTime):
            dtype = self.Dtype.DATE_TIME
        elif isinstance(self.column.type, sqlalchemy.Boolean):
            dtype = self.Dtype.BOOLEAN
        else:
            raise TypeError(
                "Unknown column type '{0}', for {1}({2})".format(
                    self.column.type.__class__.__name__,
                    self.column.table.name,
                    self.column.name
                )
            )

        return dtype, max_len


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _BaseColumnInfo(object):
    """Determine the basic data types in a set of columns.

    Parameters
    ----------
    column_representations : `list` of `any`
        The representation of all columns in a table class.
    null_values : `list` of `str` or `NoneType`, optional,
    default: `[NoneType, '']`
        Values that are considered to be empty values in the test data.
        ints/floats are not supported here.
    str_fudge_factor : `float`, optional, default: `0`
        Increase the string lengths by this proportion.
    str_round_to : `int`, optional, default: `0`
        Round string lengths to the nearest ``str_round_to``.
    str_text_cut : `int`, optional, default: `20000`
        The length in characters that VARCHARs are converted to TEXT fields.
    """
    COLUMN_CLASS = None
    """The column class to use, override this (`NoneType`).
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, column_representations, null_values=[None, ''],
                 str_fudge_factor=0, str_round_to=0, str_text_cut=20000):
        self._cols = column_representations
        self._null_values = null_values
        self.str_fudge_factor = str_fudge_factor
        self.str_round_to = str_round_to
        self.str_text_cut = str_text_cut

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def columns(self):
        """Get the column objects that store the data
        (`list` of `any`).
        """
        return self._cols


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ColumnInfo(_BaseColumnInfo):
    """Determine the basic data types in a set of columns.

    Parameters
    ----------
    column_representations : `list` of `str`
        The column names that we want to analyse.
    **kwargs
        Any other keyword arguments passed to the parent class.

    Notes
    -----
    Currently this only supports detection of floats, ints and strings. It also
    gives the max length and if the file contains any NULL values (which can be
    changed).
    """
    COLUMN_CLASS = FileColumn
    """The column class to use (`class`).
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, column_representations, **kwargs):
        # Initialise to floats as we will disprove this
        col_names = [
            self.COLUMN_CLASS(i, dtype=FileColumn.Dtype.FLOAT, nullable=False,
                              **kwargs)
            for i in column_representations
        ]
        self._nrows = 0
        super().__init__(col_names, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def nrows(self):
        """Get the number of rows that have been processed (`int`)
        """
        return self._nrows

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_row(self, row):
        """Add a row of data to use in data type detection.

        Parameters
        ----------
        row : `list` of `str`
            A row to process. It must not be shorter than the number of
            columns. If longer this will not raise an error but additional
            columns are ignored. It is expected that the row data is in the
            same order as the column names.

        Raises
        ------
        IndexError
            If the row is shorter than the number of columns.
        """
        self._nrows += 1
        for idx, col in enumerate(self._cols):
            try:
                cell = row[idx]
            except IndexError as e:
                raise IndexError(
                    f"not enough columns in row: {len(row)} vs. "
                    f"{len(self._cols)} at row: {self._nrows}"
                ) from e
            col.test_value(cell)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class TableColumnInfo(_BaseColumnInfo):
    """Determine the basic data types in a set of columns.

    Parameters
    ----------
    columns : `list` of `sqlalchemy.Column`
        The reflected SQLAlchemy columns.
    **kwargs
        Any other keyword arguments passed to the parent class.
    """
    COLUMN_CLASS = TableColumn
    """The column class to use (`class`).
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, column_representations, **kwargs):
        cols = []
        # Initialise to floats as we will disprove this
        for i in column_representations:
            c = self.COLUMN_CLASS(i)
            cols.append(c)
        super().__init__(cols, **kwargs)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Schema(object):
    """A representation of the full database schema.

    Parameters
    ----------
    module_name : `str`, optional, default: `NoneType`
        The module path. This is used to fully reference the data types for
        the relationship classes.
    verbose : `bool`, optional, default: `False`
        Show progress on defining tables.
    table_info_path : `str`, optional, default: `NoneType`
        The path to a table info file, that contains additional or custom
        information in the tables in the database.
    column_info_path : `str`, optional, default: `NoneType`
        The path to a column info file, that contains additional or custom
        information in the columns in the database.
    inset : `int`, optional, default: `4`
        The number of spaces that is required for a single level
        indentation.
    width : `int`, optional, default: `79`
        The maximum code code width.
    level : `int`, optional, default: `0`
        The indentation level for the ORM code.
    relationship_class : `str`, optional, default: `relationship`
        The name of the SQLAlchemy relationship class name. This can be changed
        to allow for aliases in the case where one of the database columns
        overrides the relationship class name.
    column_class : `str`, optional, default: `Column`
        The name of the SQLAlchemy column class name. This can be changed
        to allow for aliases in the case where one of the database columns
        overrides the column class name.
    str_fudge_factor : `float`, optional, default: `0`
        Increase the string lengths by this proportion.
    str_round_to : `int`, optional, default: `0`
        Round string lengths to the nearest ``str_round_to``.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, module_name=None, verbose=False, table_info_path=None,
                 column_info_path=None, inset=4, width=79, level=0,
                 relationship_class=_RELATIONSHIP_CLASS,
                 column_class=COLUMN_CLASS, str_fudge_factor=0,
                 str_round_to=0):
        self.module_name = module_name
        self.verbose = verbose
        self.inset = inset
        self.width = width
        self.level = level
        self.column_class = column_class
        self.relationship_class = relationship_class
        self.str_fudge_factor = str_fudge_factor
        self.str_round_to = str_round_to
        self._table_info = []
        self._column_info = dict()
        self._tables = []
        self._orm_code_cache = None

        # Make sure the module name prefix is set correctly
        if self.module_name is not None and not self.module_name.endswith('.'):
            self.module_name += '.'

        # Initialise the table and column info if they exist
        if table_info_path is not None:
            self.set_table_info(table_info_path)

        if column_info_path is not None:
            self.set_column_info(column_info_path)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_table(self, table):
        """Add a table object to the schema.

        Parameters
        ----------
        table : `list` of (`sqlalchemy_config.orm_code.FileInfo` or \
        `sqlalchemy_config.orm_code.TableInfo`)
        """
        if table.table_name in [i.table_name for i in self._tables]:
            raise ValueError(
                f"Table with name already exists in schema: {table.table_name}"
            )

        # Make sure the cache is clear
        self.clear_orm_code_cache()

        # Make sure the table has the correct module name set
        table.module_name = self.module_name
        self._tables.append(table)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_table_info(self, table_info_path):
        """Set the table info from a table info file.

        Parameters
        ----------
        table_info_path : `str`
            The path to the table info file.

        Raises
        ------
        KeyError
            If the ``table_name`` column can't be found.
        """
        self.clear_orm_code_cache()
        self._table_info = []

        with open(table_info_path, 'rt') as infile:
            reader = csv.DictReader(infile, delimiter=_DELIMITER)
            for row in reader:
                row = dict(
                    [
                        (k, v) if len(v.strip()) > 0 else (k, None)
                        for k, v in row.items()
                    ]
                )
                try:
                    row[TABLE_NAME_COL]
                except KeyError as e:
                    raise KeyError(
                        f"can't find {TABLE_NAME_COL} in table info file"
                    ) from e
                self._table_info.append(row)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_column_info(self, column_info_path):
        """Set the column info from a column info file..

        Parameters
        ----------
        column_info_path : `str`
            The path to a column info file, that contains additional or custom
            information in the tables in the database.

        Raises
        ------
        KeyError
            If the ``table_name`` or the ``column_name`` column can't be
            found.
        """
        self.clear_orm_code_cache()
        self._column_info = []

        with open(column_info_path, 'rt') as infile:
            reader = csv.DictReader(infile, delimiter=_DELIMITER)
            for row in reader:
                row = dict(
                    [
                        (k, v) if len(v.strip()) > 0 else (k, None)
                        for k, v in row.items()
                    ]
                )
                try:
                    row[TABLE_NAME_COL]
                    row[COLUMN_NAME_COL]
                except KeyError as e:
                    raise KeyError(
                        f"can't find {TABLE_NAME_COL}/{COLUMN_NAME_COL} "
                        "in table info file"
                    ) from e
                self._column_info.append(row)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_orm_code(self):
        """Get the ORM code for the whole schema/module. If the state of the
        schema has not changed then a cached copy of the orm code is returned.

        Returns
        -------
        orm_code : `str`
            The Python code for the ORM module.
        """
        if self._orm_code_cache is not None:
            return self._orm_code_cache

        # Now process everything
        self._process_tables()

        orm = '"""SQLAlchemy ORM module.\n"""\n'
        orm += "from sqlalchemy.ext.declarative import declarative_base\n"

        orm += "from sqlalchemy import (\n    "
        orm += ",\n    ".join(self._get_imports())
        orm += "\n)\n"

        as_code = _RELATIONSHIP_CLASS
        if self.relationship_class != _RELATIONSHIP_CLASS:
            as_code = f"{_RELATIONSHIP_CLASS} as {self.relationship_class}"

        orm += f"from sqlalchemy.orm import {as_code}\n\n"
        orm += "Base = declarative_base()\n\n"

        orm += _PRINT_FUNC
        orm += "\n"
        for i in self._tables:
            orm += i.orm_code(
                inset=self.inset, width=self.width, level=self.level,
                column_class=self.column_class,
                relationship_class=self.relationship_class
            )
            orm += "\n"

        # Remove any extra trailing newlines
        orm = re.sub(r'\n$', '', orm)

        # Now cache the orm code
        self._orm_code_cache = orm
        return orm

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _process_tables(self):
        """Process all the table data.
        """
        tqdm_kwargs = dict(
            desc="[info] processing files...", unit=" table/file(s)",
            disable=not self.verbose
        )

        for i in tqdm(self._tables, **tqdm_kwargs):
            i.set_dtypes(verbose=self.verbose)

        zero_col = []
        one_col = 0
        for i in self._tables:
            if len(i.info.columns) == 0:
                zero_col.append(i.name)
            if len(i.info.columns) == 1:
                one_col += 1

        # If all files have 1 column then this is suspicious
        if one_col == len(self._tables):
            warnings.warn(
                "all files have a single column, is your delimiter correct?"
            )

        # Files must have at least one column
        if len(zero_col) > 0:
            warnings.warn(
                "file(s) have 0 columns, is your delimiter correct?"
                " {0}".format(
                    ",".join(zero_col)
                )
            )

        self._update_table_info()
        self._update_column_info()
        self._update_relationships()
        self._set_aliases()
        self._check_dup_cols()
        self._check_dup_rels()
        self._check_same_rel_col()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _set_aliases(self):
        """Make sure relationship/column class aliases are set.
        """
        rel_alias = (
            ['rel'] + [
                _RELATIONSHIP_CLASS[:-1*i]
                for i in range(1, len(_RELATIONSHIP_CLASS))
            ]
        )
        col_alias = (
            ['col'] + [
                COLUMN_CLASS[:-1*i] for i in range(1, len(COLUMN_CLASS))
            ]
        )
        col_alias_idx = 0
        rel_alias_idx = 0

        rc = _RELATIONSHIP_CLASS
        cc = COLUMN_CLASS
        test_alias = True
        while test_alias is True:
            test_alias = False
            for t in self._tables:
                if t.table_name == cc:
                    test_alias = True
                    cc = col_alias[col_alias_idx]
                    col_alias_idx += 1
                if t.table_name == rc:
                    test_alias = True
                    rc = rel_alias[rel_alias_idx]
                    rel_alias_idx += 1
                for c in t.info.columns:
                    if c.name == cc:
                        test_alias = True
                        cc = col_alias[col_alias_idx]
                        col_alias_idx += 1
                    if c.name == rc:
                        test_alias = True
                        rc = rel_alias[rel_alias_idx]
                        rel_alias_idx += 1
        self.column_class = cc
        self.relationship_class = rc

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_imports(self):
        """Get the SQLAlchemy imports
        """
        col = self.column_class
        if col != COLUMN_CLASS:
            col = f"{COLUMN_CLASS} as {col}"

        imports = set([col, "ForeignKey", "Sequence"])

        for t in self._tables:
            for c in t.info.columns:
                imports.add(c.sqlalchemy_class_str)
        return sorted(imports)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _check_dup_cols(self):
        """Check for duplicated column names, this is an error.
        """
        for t in self._tables:
            cols = set()
            for c in t.info.columns:
                if c.name in cols:
                    raise ValueError(
                        f"duplicate column name: {t.table_name}({c.name})"
                    )
                cols.add(c.name)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _check_same_rel_col(self):
        """Check for columns and relationships that have the same name. In
        these cases the relationships are renamed.
        """
        suffix = ['_rel'] + list(range(1, 100))

        for t in self._tables:
            cols = [c.name for c in t.info.columns]
            for r, idx in t.relationships:
                tn = None
                update = None
                if idx == 0:
                    tn = r.first_rel_name
                    update = r.set_first_rel_name
                elif idx == 1:
                    tn = r.second_rel_name
                    update = r.set_second_rel_name
                else:
                    raise IndexError(f"unknown idx: {idx}")

                idx = 0
                while tn in cols:
                    tn = f"{tn}{suffix[idx]}"
                    update(tn)
                    idx += 1

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _check_dup_rels(self):
        """Check for duplicated relationships, in these cases the relationships
        are suffixed, flagged and told to use a primary join condition.
        """
        for t in self._tables:
            rels = dict()
            for r, idx in t.relationships:
                tn = None
                update = None
                if idx == 0:
                    tn = r.first_rel_name
                    update = r.set_first_rel_name
                elif idx == 1:
                    tn = r.second_rel_name
                    update = r.set_second_rel_name
                else:
                    raise IndexError(f"unknown idx: {idx}")

                try:
                    rels[tn].append(r)
                    update(
                        f"{tn}_{len(rels[tn])}"
                    )
                    for i in rels[tn]:
                        i.flag = True
                        i.use_primary_join = True
                except KeyError:
                    rels[tn] = [r]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _update_table_info(self):
        """Update the tables with any user defined table-info.
        """
        tables = dict([(i.table_name, i) for i in self._tables])

        for i in self._table_info:
            try:
                t = tables[i[TABLE_NAME_COL]]

                try:
                    cn = i[TABLE_CLASS_NAME_COL]
                    if cn is not None:
                        t.class_name = cn
                except KeyError:
                    pass

                try:
                    tn = i[TABLE_NAME_NEW_COL]
                    if tn is not None:
                        t.table_name = tn

                    # Loop through all other tables looking for FK references
                    # and change them
                    for j in tables.values():
                        for c in j.info.columns:
                            for idx in range(len(c.foreign_keys)):
                                fk_t, fk_c = c.foreign_keys[idx]
                                if fk_t == i[TABLE_NAME_COL]:
                                    c.foreign_keys[idx] = \
                                        (i[TABLE_NAME_NEW_COL], fk_c)
                except KeyError:
                    pass

                try:
                    doc = i[TABLE_DOC_COL]
                    if doc is not None:
                        t.doc = doc
                except KeyError:
                    pass
            except KeyError as e:
                raise KeyError(
                    f"unknown table in info: {i[TABLE_NAME_COL]}"
                ) from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _update_column_info(self):
        """Update the columns with any user defined column-info.
        """
        tables = dict()
        for t in self._tables:
            columns = dict()
            for c in t.info.columns:
                columns[c.name] = c
            tables[t.table_name] = columns

        for i in self._column_info:
            try:
                t = tables[i[TABLE_NAME_COL]]
            except KeyError as e:
                raise KeyError(
                    "unknown table in info, did you rename "
                    f"it? {i[TABLE_NAME_COL]}"
                ) from e

            try:
                c = t[i[COLUMN_NAME_COL]]
            except KeyError as e:
                raise KeyError(
                    f"unknown column in '{i[TABLE_NAME_COL]}' table: "
                    f"'{i[COLUMN_NAME_COL]}'"
                ) from e

            try:
                cn = i[COLUMN_NAME_NEW_COL]
                if cn is not None:
                    c.name = cn

                    # Loop through all other tables looking for FK references
                    # and change them
                    for j in tables.values():
                        for cl in j.info.columns:
                            for idx in range(cl.foreign_keys):
                                fk_t, fk_c = cl.foreign_keys[idx]
                                if fk_t == i[TABLE_NAME_COL] and \
                                   fk_c == i[COLUMN_NAME_COL]:
                                    cl.foreign_keys[idx] = \
                                        (i[TABLE_NAME_COL],
                                         i[COLUMN_NAME_NEW_COL])
            except KeyError:
                pass

            try:
                fk = i[COLUMN_FOREIGN_KEY]
                if fk is not None and len(fk.strip()) > 0:
                    c.foreign_keys.append([i.strip() for i in fk.split('.')])
            except KeyError:
                pass

            try:
                dt = i[COLUMN_DTYPE]
                if dt is not None:
                    c.dtype = dt
            except KeyError:
                pass

            try:
                ml = i[COLUMN_MAX_LEN]
                if ml is not None:
                    c.max_len = int(ml)
            except KeyError:
                pass

            try:
                d = i[COLUMN_DOC]
                if d is not None:
                    c.doc = d
            except KeyError:
                pass

            bool_info = [
                (COLUMN_PRIMARY_KEY, 'primary_key'),
                (COLUMN_INDEX, 'index'),
                (COLUMN_NULLABLE, 'nullable'),
                (COLUMN_UNIQUE, 'unique')
            ]
            for col, attr in bool_info:
                try:
                    d = i[col]
                    if d is not None and len(d) > 0:
                        setattr(c, attr, bool(int(d)))
                except KeyError:
                    pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _update_relationships(self):
        """Attempt to define relationships based on foreign keys.
        """
        table_map = dict()
        for t in self._tables:
            table_map[t.table_name] = t

        for t1 in self._tables:
            for c1 in t1.info.columns:
                for tn2, cn2 in c1.foreign_keys:
                    t2 = table_map[tn2]
                    ColumnRelationship((t1, c1.name), (t2, cn2))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def clear_orm_code_cache(self):
        """Clear the ORM code cache.
        """
        self._orm_code_cache = None


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _BaseInfo(object):
    """A base representation of a table/SQLAlchemy class.

    Parameters
    ----------
    table_representation : `any`
        A representation of a table.
    table_name : `str`, optional, default: `NoneType`
        An alternative table name.
    class_name : `str`, optional, default: `NoneType`
        An alternative class name.
    doc : `str`, optional, default: `NoneType`
        Any table documentation. This will be added to the Notes section in the
        docstring.
    module_name : `str`, optional, default: `NoneType`
        An optional module name to add to relationship docstring data types.
    str_fudge_factor : `float`, optional, default: `0`
        Increase the string lengths by this proportion.
    str_round_to : `int`, optional, default: `0`
        Round string lengths to the nearest ``str_round_to``.
    str_text_cut : `int`, optional, default: `20000`
        The length in characters that VARCHARs are converted to TEXT fields.
    """
    COLUMN_HOLDER = None
    """The column holder class override this (`NoneType`).
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, table_representation, table_name=None, class_name=None,
                 doc=None, module_name=None, str_fudge_factor=0,
                 str_round_to=0, str_text_cut=20000):
        self._table_rep = table_representation
        self._table_name = table_name
        self._class_name = class_name
        self.str_fudge_factor = str_fudge_factor
        self.str_round_to = str_round_to
        self.doc = doc
        self.info = None
        self._relationships = []
        self.module_name = module_name or ""
        if len(self.module_name) > 0:
            self.module_name = f"{self.module_name}."
        self.str_text_cut = str_text_cut

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def representation(self):
        """Get the table representation (`any`)
        """
        return self._table_rep

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def table_name(self):
        """Get the table name for the table (`str`)
        """
        if self._table_name is None:
            raise ValueError("table name must be defined")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @table_name.setter
    def table_name(self, table_name):
        """Set the table name represented by the file (`str`)
        """
        self._table_name = table_name

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def class_name(self):
        """Get the class name represented by the file (`str`)
        """
        if self._class_name is None:
            class_name = self.table_name
            return "".join([i.title() for i in class_name.split('_')])
        return self._class_name

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @class_name.setter
    def class_name(self, class_name):
        """Set the class name represented by the file (`str`)
        """
        self._class_name = class_name

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def relationships(self):
        """Get all the relationships in the order they were added (`list` of
        `sqlalchemy_config.orm_code.ColumnRelationship`).
        """
        return list(self._relationships)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_relationship(self, rel):
        """Add a relationship to the table.

        Parameters
        ----------
        rel : `sqlalchemy_config.orm_code.ColumnRelationship`
            A relationship to add to the table.

        Raises
        ------
        ValueError
            If the table in the relationship does not match, this table.
        """
        if rel.t1 != self and rel.t2 != self:
            raise ValueError("table not in relationship")

        idx = [rel.t1, rel.t2].index(self)
        rel = (rel, idx)

        # Won't add duplicated relationships
        if rel not in self._relationships:
            self._relationships.append(rel)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_dtypes(self, verbose=False):
        """Analyse the table representation and set the data types and columns.

        Parameters
        ----------
        verbose : `bool`, optional, default: `False`
            Report progress.

        Notes
        -----
        This should be overridden.
        """
        pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def orm_code(self, inset=4, width=79, level=0, column_class=COLUMN_CLASS,
                 relationship_class=_RELATIONSHIP_CLASS):
        """Return the ORM code for the SQLAlchemy class.

        Parameters
        ----------
        inset : `int`, optional, default: `4`
            The number of spaces that is required for a single level
            indentation.
        width : `int`, optional, default: `79`
            The maximum code code width.
        level : `int`, optional, default: `0`
            The indentation level for the ORM code.
        relationship_class : `str`, optional, default: `relationship`
            The name of the SQLAlchemy relationship class name. This can be
            changed to allow for aliases in the case where one of the database
            columns overrides the relationship class name.
        column_class : `str`, optional, default: `Column`
            The name of the SQLAlchemy column class name. This can be changed
            to allow for aliases in the case where one of the database columns
            overrides the column class name.

        Returns
        -------
        orm_code : `str`
            An ORM class definition.
        """
        one = " " * level
        two = " " * (level + (1 * inset))
        code = [
            "\n# " + ("@" * (width - 2)),
            f"{one}class {self.class_name}(Base):",
            f'{two}"""A representation of the ``{self.table_name}``'
            f' table.\n'
        ]

        code.append(f"{two}Parameters\n{two}----------")
        for c in self.info.columns:
            code.append(
                c.docstring_code(inset=inset, width=width, level=level)
            )

        for rel, idx in self._relationships:
            reldoc = rel.docstring_code(
                inset=inset, width=width, level=level,
                module_name=self.module_name
            )[idx]
            code.append(
                f'{reldoc}'
            )

        if self.doc is not None:
            code.append(f"\n{two}Notes\n{two}-----")
            code.extend(
                textwrap.wrap(
                    self.doc, width=width, initial_indent=two,
                    subsequent_indent=two
                )
            )
        code.append(f'{two}"""')
        code.append(f'{two}__tablename__ = "{self.table_name}"\n')
        for i in self.info.columns:
            colorm = i.orm_code(
                inset=inset, width=width, level=level,
                column_class=column_class
            )
            code.append(
                f'{two}{colorm}'
            )

        if len(self._relationships) > 0:
            tilda_len = int((width - (17 + len(two)))/2)
            code.append(
                f"\n{two}# {'~' * tilda_len} relationships {'~' * tilda_len}"
            )
        for rel, idx in self._relationships:
            if rel.flag is True:
                code.append(f"{two}# TODO: FLAG for checking")

            relorm = rel.orm_code(
                inset=inset, width=width, level=level,
                relationship_class=relationship_class
            )[idx]
            code.append(f'{two}{relorm}')

        code.append(_REPR_METHOD)

        return "\n".join(code)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class FileInfo(_BaseInfo):
    """A container class for the options of an input file.

    Parameters
    ----------
    table_representation : `str`
        The input file path.
    encoding : `str`, optional, default: 'utf8'
        The encoding of the file.
    comment : `str`, optional, default: `#`
        Comment characters.
    skiplines : `int`, optional, default: `0`
        Skip this many lines before reading the file.
    delimiter : `str`, optional, default: `\t`
        The default delimiter for the file.
    no_header : `bool`, optional, default: `False`
        The first row of the file being tested is not a header row.
    header_cols : `list` of `str`, optional, default: `NoneType`
        Alternative header column name to use, instead of the header in the
        file.
    sample : `int`, optional, default: `-1`
        The number of data rows in the input file to test for data types. Set
        to a negative value to test all rows.
    table_name : `str`, optional, default: `NoneType`
        An alternative table name.
    class_name : `str`, optional, default: `NoneType`
        An alternative class name.
    doc : `str`, optional, default: `NoneType`
        Any table documentation. This will be added to the Notes section in the
        docstring.
    module_name : `str`, optional, default: `NoneType`
        An optional module name to add to relationship docstring data types.
    str_fudge_factor : `float`, optional, default: `0`
        Increase the string lengths by this proportion.
    str_round_to : `int`, optional, default: `0`
        Round string lengths to the nearest ``str_round_to``.
    str_text_cut : `int`, optional, default: `20000`
        The length in characters that VARCHARs are converted to TEXT fields.
    **csv_kwargs
        Any other keyword arguments passed to csv.

    Raises
    ------
    ValueError
        If skiplines < 0.
    """
    DEFAULT_DELIMITER = "\t"
    """The default input file delimiter (`str`)
    """
    DEFAULT_ENCODING = "UTF8"
    """The default input file encoding (`str`)
    """
    DEFAULT_COMMENT = "#"
    """The default input file comment line (`str`)
    """
    DEFAULT_SKIPLINES = 0
    """The default number of lines to skip (`str`)
    """
    COLUMN_HOLDER = ColumnInfo
    """The column holder class (`class`).
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, table_representation, encoding=DEFAULT_ENCODING,
                 comment=DEFAULT_COMMENT, skiplines=DEFAULT_SKIPLINES,
                 delimiter=DEFAULT_DELIMITER, no_header=False,
                 header_cols=None, sample=-1, table_name=None,
                 class_name=None, doc=None, module_name=None,
                 str_fudge_factor=0, str_round_to=0, str_text_cut=20000,
                 **csv_kwargs):
        super().__init__(table_representation, table_name=table_name,
                         class_name=class_name, doc=doc,
                         module_name=module_name,
                         str_fudge_factor=str_fudge_factor,
                         str_round_to=str_round_to,
                         str_text_cut=str_text_cut)

        self.name = pathlib.Path(self._table_rep)

        if self.name.exists() is False:
            raise IOError("input file does not exist")

        if self.name.is_file() is False:
            raise IOError("input is not a file")

        self.encoding = encoding
        self.comment = comment
        self.skiplines = skiplines
        self.delimiter = delimiter
        self.csv_kwargs = csv_kwargs
        self.no_header = no_header
        self.header_cols = header_cols
        self.sample = sample

        if self.skiplines < 0:
            raise ValueError("skiplines should be >= 0")

        # Will hold the number of rows in the file, this will be lazy obtained
        self._cols = None

        # Will hold the number of rows in the file, this will be lazy obtained
        self._nrows = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def ndata_rows(self):
        """Get the number of data rows in the file, excluding header, comments
        etc... (`int`)
        """
        if self._data_row_count is None:
            # Read will count the rows
            for i in self.read():
                pass
        return self._data_row_count

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def nrows(self):
        """Get the number of rows in the file (`int`)
        """
        if self._nrows is None:
            # Read will count the rows
            for i in self.read():
                pass
        return self._nrows

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def table_name(self):
        """Get the table name represented by the file (`str`)
        """
        if self._table_name is None:
            table_name = re.sub(
                r'[\W\s]+', '_', re.sub(
                    r'\.\w*$', '',
                    os.path.basename(str(self.name))
                )
            )
            return re.sub(r'_+', '_', table_name)
        return self._table_name

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_fobj(self):
        """Get a file object. Agnostic to gzipped.

        Returns
        -------
        fobj : `_io.TextIOWrapper` or ``
            A file object open in text mode.
        """
        if utils.is_gzip(self.name) is False:
            return open(self.name, 'rt', encoding=self.encoding)
        else:
            return gzip.open(self.name, 'rt', encoding=self.encoding)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def read(self):
        """Yield rows from the input file. This sorts comment/skiplines.

        Yields
        ------
        row : `list` of `str`
            Valid rows from the file, the first row is always the header row.
        """
        # Will hold the count of rows output (including any headers, comments)
        total_count = 0
        data_count = 0
        with self.get_fobj() as infile:
            reader = csv.reader(infile, delimiter=self.delimiter,
                                **self.csv_kwargs)
            first_row, first_row_idx = header.get_header_from_reader(
                reader, skiplines=self.skiplines, comment=self.comment
            )

            if self.no_header is True and self.header_cols is not None:
                total_count += 1
                yield self.header_cols.copy()
            elif self.no_header is True:
                total_count += 1
                yield [str(i) for i in range(len(first_row))]

            # Now the first row of the file, this could be the header
            total_count += 1
            yield first_row

            for row in reader:
                # count the rows
                total_count += 1
                row = [i.strip() for i in row]
                if row[0].startswith(self.comment):
                    continue
                data_count += 1
                yield row
            self._data_row_count = data_count
            self._nrows = total_count

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_dtypes(self, verbose=False):
        """Scan the file and check the datatypes of each of the columns.

        Parameters
        ----------
        verbose : `bool`, optional, default: `False`
            Report progress through the file.

        Notes
        -----
        Currently this only distinguishes between ``float``, ``int`` and
        ``str``. Although, ``bool`` may be possible in future.
        """
        reader = self.read()
        header = next(reader)

        info = self.COLUMN_HOLDER(
            header, str_fudge_factor=self.str_fudge_factor,
            str_round_to=self.str_round_to
        )
        fn = os.path.basename(str(self.name))
        idx = 0
        for row in tqdm(reader, disable=not verbose, unit=" rows",
                        desc=f"[info] extract dtypes: {fn}", leave=False):
            info.add_row(row)
            idx += 1
            if idx == self.sample:
                break

        self.info = info


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class TableInfo(_BaseInfo):
    """A container class for the options of an input table reflected via
    SQLAlchemy.

    Parameters
    ----------
    table_representation : `sqlalchemy.Table`
        The reflected input table.
    table_name : `str`, optional, default: `NoneType`
        An alternative table name.
    class_name : `str`, optional, default: `NoneType`
        An alternative class name.
    doc : `str`, optional, default: `NoneType`
        Any table documentation. This will be added to the Notes section in the
        docstring.
    module_name : `str`, optional, default: `NoneType`
        An optional module name to add to relationship docstring data types.
    str_fudge_factor : `float`, optional, default: `0`
        Increase the string lengths by this proportion.
    str_round_to : `int`, optional, default: `0`
        Round string lengths to the nearest ``str_round_to``.
    str_text_cut : `int`, optional, default: `20000`
        The length in characters that VARCHARs are converted to TEXT fields.
    """
    COLUMN_HOLDER = TableColumnInfo
    """The column holder class (`class`).
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, table_representation, table_name=None, class_name=None,
                 doc=None, module_name=None, str_fudge_factor=0,
                 str_round_to=0, str_text_cut=20000):
        super().__init__(table_representation, table_name=table_name,
                         class_name=class_name, doc=doc,
                         module_name=module_name,
                         str_fudge_factor=str_fudge_factor,
                         str_round_to=str_round_to,
                         str_text_cut=str_text_cut)
        self.table = table_representation

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def table_name(self):
        """Get the table name represented by the file (`str`)
        """
        if self._table_name is None:
            return self.table.name
        return self._table_name

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_dtypes(self, verbose=False):
        """Scan the file and check the datatypes of each of the columns.

        Parameters
        ----------
        verbose : `bool`, optional, default: `False`
            Report progress through the file.

        Notes
        -----
        Currently this only distinguishes between ``float``, ``int`` and
        ``str``. Although, ``bool`` may be possible in future.
        """
        self.info = self.COLUMN_HOLDER(
            self.table.columns, str_fudge_factor=self.str_fudge_factor,
            str_round_to=self.str_round_to
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ColumnRelationship(object):
    """A relationship between two table columns.

    Parameters
    ----------
    table_col1 : `tuple` of ((`sqlalchemy_config.orm_code.FileInfo` or \
    `sqlalchemy_config.orm_code.TableInfo`), `str`)
        The first table in the relationship.
    table_col2 : `tuple` of ((`sqlalchemy_config.orm_code.FileInfo` or \
    `sqlalchemy_config.orm_code.TableInfo`), `str`)
        The second table in the relationship.
    use_primary_join : `bool`, optional, default: `False`
        Should the relationship use a primary join argument? These are required
        when you have multiple foreign keys linking to a single table/column.
    flag : `bool`, optional, default: `False`
        Should the relationship be flagged for a TODO comment?
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, table_col1, table_col2, use_primary_join=False,
                 flag=False):
        self._t1_rel_name = None
        self._t2_rel_name = None
        self.flag = flag
        self.use_primary_join = use_primary_join
        self.set_first_rel(*table_col1)
        self.set_second_rel(*table_col2)

        # Store the relationships in the table
        self.t1.add_relationship(self)
        self.t2.add_relationship(self)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_first_rel(self, table, column_name):
        """Set the table/column for the first relationship.

        Parameters
        ----------
        table : `sqlalchemy_config.orm_code.FileInfo` or \
        `sqlalchemy_config.orm_code.TableInfo`
            The first table in the relationship.
        column_name : `str`
            The column name for the first relationship
        """
        self.t1, self.cn1 = table, column_name

        try:
            self.c1 = \
                [c for c in self.t1.info.columns if c.name == self.cn1][0]
        except IndexError as e:
            raise IndexError(
                f"Can't find column '{self.cn1}' in table {self.t1.table_name}"
            ) from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_second_rel(self, table, column_name):
        """Set the table/column for the second relationship.

        Parameters
        ----------
        table : `sqlalchemy_config.orm_code.FileInfo` or \
        `sqlalchemy_config.orm_code.TableInfo`
            The second table in the relationship.
        column_name : `str`
            The column name for the second relationship
        """
        self.t2, self.cn2 = table, column_name

        try:
            self.c2 = \
                [c for c in self.t2.info.columns if c.name == self.cn2][0]
        except IndexError as e:
            raise IndexError(
                f"Can't find column '{self.cn2}' in table {self.t2.table_name}"
            ) from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_first_rel_name(self, rel_name):
        """Set the relationship name for the first relationship.

        Parameters
        ----------
        rel_name : `str`
            The new name for the first relationship. Should not contain spaces.
        """
        self._t1_rel_name = rel_name

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_second_rel_name(self, rel_name):
        """Set the relationship name for the second relationship.

        Parameters
        ----------
        rel_name : `str`
            The new name for the second relationship. Should not contain
            spaces.
        """
        self._t2_rel_name = rel_name

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def first_rel_name(self):
        """Get the name of the first relationship (`str`)
        """
        if self._t1_rel_name is None:
            return self.t2.table_name
        return self._t1_rel_name

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def second_rel_name(self):
        """Get the name of the second relationship (`str`)
        """
        if self._t2_rel_name is None:
            return self.t1.table_name
        return self._t2_rel_name

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_references(self):
        """Get the ``table_name.column_name`` references for each relationship.

        Returns
        -------
        first_relationship_ref : `str`
            The relationship reference for the left side of the relationship.
        second_relationship_ref : `str`
            The relationship reference for the right side of the relationship.
        """
        return (
            f"{self.t1.table_name}.{self.cn1}",
            f"{self.t2.table_name}.{self.cn2}"
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def tables(self):
        """Get the table objects involved in the relationship.

        Returns
        -------
        first_relationship_table : (`sqlalchemy_config.orm_code.FileInfo` or \
        `sqlalchemy_config.orm_code.TableInfo`)
            The relationship reference for the left side (first) of the
            relationship.
        second_relationship_ref : (`sqlalchemy_config.orm_code.FileInfo` or \
        `sqlalchemy_config.orm_code.TableInfo`)
            The relationship reference for the right side (second) of the
            relationship.
        """
        return self.t1, self.t2

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def orm_code(self, inset=4, width=79, level=0,
                 relationship_class=_RELATIONSHIP_CLASS):
        """Return the ORM code for the SQLAlchemy class.

        Parameters
        ----------
        inset : `int`, optional, default: `4`
            The number of spaces that is required for a single level
            indentation.
        width : `int`, optional, default: `79`
            The maximum code code width.
        level : `int`, optional, default: `0`
            The indentation level for the ORM code.
        relationship_class : `str`, optional, default: `relationship`
            The relationship class name to use for the SQLAlchemy relationship
            definitions.

        Returns
        -------
        orm_code : `str`
            An ORM class definition.
        """

        kwargs = [
            f'back_populates="{self.second_rel_name}"',
            f'doc="Relationship back to ``{self.t2.table_name}``"'
        ]

        if self.use_primary_join is True:
            kwargs.append(
                f'primaryjoin="{self.t1.class_name}.{self.cn1} == '
                f'{self.t2.class_name}.{self.cn2}"'
            )

        t1_orm = "{0} = {3}(\n{1}\n{2})".format(
            self.first_rel_name,
            format_args(
                [f'"{self.t2.class_name}"'],
                kwargs,
                indent=8, width=79
            ),
            " " * 4, relationship_class
        )

        kwargs = [
            f'back_populates="{self.first_rel_name}"',
            f'doc="Relationship back to ``{self.t1.table_name}``."'
        ]

        if self.use_primary_join is True:
            kwargs.append(
                f'primaryjoin="{self.t1.class_name}.{self.cn1} == '
                f'{self.t2.class_name}.{self.cn2}"'
            )

        t2_orm = "{0} = {3}(\n{1}\n{2})".format(
            self.second_rel_name,
            format_args(
                [f'"{self.t1.class_name}"'],
                kwargs,
                indent=8, width=79
            ),
            " " * 4, relationship_class
        )
        return t1_orm, t2_orm

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def docstring_code(self, inset=4, width=79, level=0, module_name=None):
        """Return the formatted docstring code.

        Parameters
        ----------
        inset : `int`, optional, default: `4`
            The number of spaces that is required for a single level
            indentation.
        width : `int`, optional, default: `79`
            The maximum code code width.
        level : `int`, optional, default: `0`
            The indentation level for the ORM code.
        module_name : `str`, optional, default: `NoneType`
            An optional module name to add to relationship docstring data
            types.

        Returns
        -------
        relationship_docstring_code : `str`
            The column docstring code to go into the numpy docstring.
        """
        module_name = module_name or ""

        t1_dt = _wrap_docstring_dtype(
            f"{self.first_rel_name} : `{module_name}{self.t2.class_name}`"
            ", optional, default: `NoneType`",
            width=width, indent_chars=(_INDENT_CHAR * inset)
        )
        t1_doc = f"        Relationship with ``{self.t2.table_name}``."

        t2_dt = _wrap_docstring_dtype(
            f"{self.second_rel_name} : `{module_name}{self.t1.class_name}`"
            ", optional, default: `NoneType`",
            width=width, indent_chars=(_INDENT_CHAR * inset)
        )
        t2_doc = f"        Relationship with ``{self.t1.table_name}``."

        return f"{t1_dt}\n{t1_doc}", f"{t2_dt}\n{t2_doc}"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    if args.template_prefix is not None:
        raise NotImplementedError("template output not yet implemented")

    try:
        with stdopen.open(args.outfile, 'wt') as outfile:
            schema = Schema(
                module_name=args.module_name,
                verbose=log.progress_verbose(verbose=args.verbose),
                table_info_path=args.table_info,
                column_info_path=args.column_info
            )
            if args.reflect is False:
                for i in args.infiles:
                    schema.add_table(
                        FileInfo(
                            i, encoding=args.encoding, comment=args.comment,
                            skiplines=args.skip_lines, sample=args.sample,
                            delimiter=args.delimiter,
                            str_fudge_factor=args.str_fudge_factor,
                            str_round_to=args.str_round_to,
                            str_text_cut=args.str_text_cut
                        )
                    )
            else:
                # Get a sessionmaker to create sessions to interact with the
                # database
                sm = cfg.get_new_sessionmaker(
                    args.infiles[0], conn_prefix=_DEFAULT_PREFIX,
                    config_arg=args.config, config_env=None,
                    config_default=_DEFAULT_CONFIG, exists=True
                )
                # Get the session to query/load
                session = sm()
                try:
                    for i in reflect_tables(
                            session,
                            str_fudge_factor=args.str_fudge_factor,
                            str_round_to=args.str_round_to,
                            str_text_cut=args.str_text_cut
                    ):
                        schema.add_table(i)
                finally:
                    session.close()
            outfile.write(schema.get_orm_code())

        if args.template_prefix is not None:
            # Output template files
            pass
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        log.log_end(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        'infiles', type=str, nargs='+',
        help="The path to one or more input tables (can be gzip compressed)"
    )
    parser.add_argument(
        '-o', '--outfile', type=str,
        help="The path to the output file (if not given will default to"
        " STDOUT)"
    )
    parser.add_argument(
        '-d', '--delimiter', type=str, default="\t",
        help="The delimiter of the input files"
    )
    parser.add_argument(
        '-R', '--reflect', action="store_true",
        help="This tells the script that the infiles argument is not files but"
        " a database connection string, or reference to a connection in a "
        "config file. In this case only a single infile is expected."
    )
    parser.add_argument(
        '-e', '--encoding', type=str, default="UTF8",
        help="The encoding of the input files"
    )
    parser.add_argument(
        '-s', '--skip-lines', type=int, default=0,
        help="The skip this many lines before processing the header"
    )
    parser.add_argument(
        '-c', '--comment', type=str, default="#",
        help="Skip lines that start with this string"
    )
    parser.add_argument(
        '-S', '--sample', type=int, default=-1,
        help="The number of lines to sample from the input file to determine"
        " data types"
    )
    parser.add_argument(
        '--str-fudge-factor', type=float, default=0,
        help="The fudge factor to apply to string (Varchar) max lengths"
    )
    parser.add_argument(
        '--str-round-to', type=int, default=0,
        help="Round string (Varchar) data type length to this value. This is"
        " applied after any fudge factors are applied."
    )
    parser.add_argument(
        '--str-text-cut', type=int, default=20000,
        help="The cut point in character length where VARCHAR are stored as"
        " text. This is assessed after any fudge factors/rounding has been"
        " applied."
    )
    parser.add_argument(
        '--table-info', type=str,
        help="The path to a table info file that contains custom table "
        "information. The table info file must have at least a "
        "``table_name`` column but can also have a ``new_table_name`` column"
        " that can be used to alter the auto-detected table name. It can also"
        " have a ``class_name`` column that can be used to alter the "
        "auto-generated class name. Finally, a ``table_doc`` column may be "
        "provided to specific documentation on the table. This will be"
        "written to the class docstring Notes section. The columns can be in"
        " any order."
    )
    parser.add_argument(
        '--column-info', type=str,
        help="The path to a column info file that can be used to supply"
        "column descriptions and override column parameters that are "
        "automatically identified by the script. The file must contain a"
        " ``table_name`` and a ``column_name`` columns. The other columns are"
        " ``dtype`` (data type, VARCHAR, INTEGER etc...), ``max_len`` "
        "(varchar length integer), ``primary_key`` (0/1), ``index`` (0/1),"
        " ``is_nullable`` (0/1), ``unique`` (0/1), ``column_doc`` (string). "
        "Columns can be given in any order."
    )
    parser.add_argument(
        '-C', '--config',
        type=str,
        default="~/{0}".format(
            os.path.basename(_DEFAULT_CONFIG)
        ),
        help="The location of the config file"
    )
    parser.add_argument(
        '--module-name', type=str,
        help="The name of the ORM module in it's final package context."
    )
    parser.add_argument(
        '--template-prefix', type=str,
        help="Set this if you want to output template info files that you can"
        " work on and use as input later (not yet implemented)."
    )
    parser.add_argument(
        '-v', '--verbose',  action="count", default=0,
        help="give more output, -vv will turn on progress monitoring"
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # Because the default is ~/ for Sphinx docs
    args.config = os.path.expanduser(args.config)

    if args.reflect is True and len(args.infiles) != 1:
        raise ValueError(
            "expect only a single database connection argument when reflecting"
        )
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def reflect_tables(session, module_name=None, str_fudge_factor=0,
                   str_round_to=0, str_text_cut=20000):
    """Reflects all tables to declaratives.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQL Alchemy session object. Must have a valid get_bind() call.
    module_name : `str`, optional, default: `NoneType`
        The final name for the ORM module in it's final package context.
    str_fudge_factor : `float`, optional, default: `0`
        Increase the string lengths by this proportion.
    str_round_to : `int`, optional, default: `0`
        Round string lengths to the nearest ``str_round_to``.
    str_text_cut : `int`, optional, default: `20000`
        The length in characters that VARCHARs are converted to TEXT fields.

    Returns
    -------
    tables : `list` of `sqlalchemy_config.orm_code.TableInfo`
        Tables objects that have been reflected from the database represented
        by the session.
    """
    # create an unbound base our objects will inherit from
    Base = declarative_base()

    metadata = sqlalchemy.MetaData()
    Base.metadata = metadata
    metadata.reflect(bind=session.get_bind())
    tables = []

    for tablename, tableobj in metadata.tables.items():
        if tablename.startswith('sqlite_stat'):
            # Ignore SQLite tables
            continue

        tables.append(
            TableInfo(
                tableobj, module_name=module_name,
                str_fudge_factor=str_fudge_factor,
                str_round_to=str_round_to,
                str_text_cut=str_text_cut
            )
        )
    return tables


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def format_args(args, kwargs, indent=0, width=79):
    """Format the text of args and kwargs that will be given to a function
    call.

    Parameters
    ----------
    args : `list` of `str`
        The positional arguments.
    kwargs : `list` of `str`
        The format of the strings for kwargs will be parameter=value.
    indent : `int`, optional, default: `0`
        The indention of any wrapped parameters.
    width : `int`, optional, default: `79`
        The ideal width of the line.
    """
    all_lines = []
    joiner = ", "
    line = " " * indent
    for idx, i in enumerate(args):
        joiner_char = joiner
        try:
            args[idx+1]
        except IndexError:
            if len(kwargs) == 0:
                joiner_char = ""
        if (len(line) + len(i) + len(joiner_char)) > width \
           and len(line) > indent:
            # New line
            all_lines.append(line)
            line = (" " * indent) + i + joiner_char
        else:
            line += (i + joiner_char)

    # Process the "keyword" arguments
    for idx, i in enumerate(kwargs):
        joiner_char = joiner
        try:
            kwargs[idx+1]
        except IndexError:
            # Last keyword argument, has no comma after
            joiner_char = ""

        # Adding the next argument would make the line too long
        if (len(line) + len(i) + len(joiner_char)) > width \
           and len(line) > indent:
            # New line
            all_lines.append(line)
            line = (" " * indent) + i + joiner_char

            if len(line) > width and re.search(r'=".+"', line):
                line = textwrap.wrap(
                    line, width=79-2, initial_indent="",
                    subsequent_indent=(" " * indent),
                    drop_whitespace=False
                )
                for idx in range(1, len(line)):
                    line[idx] = re.sub(
                        r'\s{{{0}}}'.format(indent),
                        (" " * indent) + '"',
                        line[idx]
                    )
                for idx in range(0, len(line) - 1):
                    line[idx] = f'{line[idx]}"'

                line = "\n".join([i.rstrip() for i in line])
        else:
            line += (i + joiner_char)
    all_lines.append(line)
    return "\n".join([i.rstrip() for i in all_lines])


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _wrap_docstring_dtype(dtype, width=79, indent_chars="    ",
                          return_str=True):
    """Wrap a docstring datatype line if needed. This will ensure the
    continuation character is placed correctly.

    Parameters
    ----------
    dtype : `str`
        The Numpy docstring data type row.
    width : `int`, optional, default: `79`
        The maximum code code width.
    indent_chars : `str`, optional, default: `    `
        The indent characters for the docstring continuation row.
        indentation.
    return_str : `bool`, optional, default: `True`
        Return a string with \n or a list of each line.

    Returns
    -------
    docstring_dtype_code : `str` or `list`
        The column docstring datatype definition.
    """
    base_param = textwrap.wrap(
        dtype, width=width - 2, initial_indent=indent_chars,
        subsequent_indent=indent_chars
    )
    if len(base_param) > 1:
        for i in range(len(base_param) - 1):
            base_param[i] = f'{base_param[i]} \\'

    if return_str is True:
        return "\n".join(base_param)
    return base_param


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def round_to_nearest(x, fudge_factor=0.1, round_to=50):
    """Found an integer to the nearest multiple of ``round_to`` after applying
    a fudge factor.

    Parameters
    ----------
    x : `int`
        The number to round.
    fudge_factor : `float`, optional, default: `0.1`
        A proportion of of x to adjust x by, so 0.1 is add 10% to x.
    round_to : `int`, optional, default: `50`
        Round x to the nearest ``round_to``.

    Returns
    -------
    rounded_x : `int`
        x with the ``fudge_factor`` applied and rounded to ``round_to``.

    Notes
    -----
    if round to is 0 then x will be returned.
    """
    try:
        return math.ceil((x * (1 + fudge_factor)) / round_to) * round_to
    except ZeroDivisionError:
        return x


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
