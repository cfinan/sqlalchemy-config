# Getting Started with sqlalchemy-config

__version__: `0.1.0a1`

The sqlalchemy-config package is a toolkit to handle database configuration parameters that are stored in a config file. This can locate, read, and extract the config parameters and create a sessionmaker object from them that is bound to the engine object.

There is [online](https://cfinan.gitlab.io/sqlalchemy-config/index.html) documentation for sqlalchemy-config and offline PDF documentation can be downloaded [here](https://gitlab.com/cfinan/sqlalchemy-config/-/blob/master/resources/pdf/sqlalchemy-config.pdf).

## Installation instructions
At present, sqlalchemy-config is undergoing development and no packages exist yet on PyPy or in conda. Therefore it is recommended that it is installed in either of the two ways listed below. First, clone this repository and then `cd` to the root of the repository.

```
# Change in your package - this is not in git
git clone git@gitlab.com:cfinan/sqlalchemy-config.git
cd sqlalchemy-config
```

### Installation not using any conda dependencies
If you are not using conda in any way then install the dependencies via `pip` and install sqlalchemy-config as an editable install also via pip:

Install dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

For an editable (developer) install run the command below from the root of the sqlalchemy-config repository (or drop `-e` for static install):
```
python -m pip install -e .
```

### Installation using conda
A conda build is also available for Python v3.8/v3.9 on linux-64/osX-64.
```
conda install -c cfin sqlalchemy-config
```

If you are using conda and require anything different then please install with pip and install the dependencies with the environments specified in `resources/conda_env`. There is also a build recipe in `./resourses/build/conda` that can be used to create new packages.

## Usage TL;DR

sqlalchemy-config is designed to handle potential database connection inputs coming from different sources. Such as a URL or file (for SQLite) being specified on the command line, or a path to a config file containing the parameters being specified as a command line arg, or in a environment variable or the route of the home directory.

The config files used in the example below have a similar structure to this see [SQLAlchemy engine from config](https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.engine_from_config):

```
[mydb_mysql]
# Make sure this is only readable by you
# Make sure password is URL encoded, see: sqlalchemy_config.encode_password
db.url = mysql+pymysql://user:password@127.0.0.1:3306/db_cfg_arg
```

```python
>>> import sqlalchemy_config as sqlc
>>> import os

>>> # This represents input fromt he command line, of a single file that will be inperpreted as SQLite
>>> url_arg = "mydb.db"

>>> # This is the section in the config file (ini) where we will find any database connection parameters
>>> config_section = 'mydb_mysql'

>>> # This is the prefix from any connection parameters in under config_section
>>> config_prefix = 'db.'

>>> # This represents an environemnt variable name in your ~/.bashrc that has the path to a config file
>>> config_env = "MYDBS"

>>> # This represents a default location for config files containing parameters
>>> config_default = os.path.join(os.environ['HOME'], "mydbs_def.cnf")

>>> The url will take priority and the file is interpreted as SQLite connection
>>> sqlc.get_sessionmaker(config_section, config_prefix, url_arg=url_arg, config_arg=config_arg, config_env=config_env, config_default=config_default)
>>> sessionmaker(class_='Session', bind=Engine(sqlite:///mydb.db), autoflush=True, autocommit=False, expire_on_commit=True)

# The URL argument can also be a fully formed connection url
>>> url_arg = "mysql+pymysql://user:password@127.0.0.1:3306/db_url_arg"
>>> sqlc.get_sessionmaker(config_section, config_prefix, url_arg=url_arg, config_arg=config_arg, config_env=config_env, config_default=config_default)
>>> sessionmaker(class_='Session', bind=Engine(mysql+pymysql://user:***@127.0.0.1:3306/db_url_arg), autoflush=True, autocommit=False, expire_on_commit=True)

# URL is not supplied so look at any conffig file command line arguments and use the section and prefix information
>>> url_arg = None
>>> sqlc.get_sessionmaker(config_section, config_prefix, url_arg=url_arg, config_arg=config_arg, config_env=config_env, config_default=config_default)
>>> sessionmaker(class_='Session', bind=Engine(mysql+pymysql://user:***@127.0.0.1:3306/db_cfg_arg), autoflush=True, autocommit=False, expire_on_commit=True)

# url and config arg not supplied so fall back to any environment variables
>>> url_arg = None
>>> config_arg = None
>>> sqlc.get_sessionmaker(config_section, config_prefix, url_arg=url_arg, config_arg=config_arg, config_env=config_env, config_default=config_default)
>>> sessionmaker(class_='Session', bind=Engine(mysql+pymysql://user:***@127.0.0.1:3306/db_env_var), autoflush=True, autocommit=False, expire_on_commit=True)

# url, config and env are missing so fall back to the default location
>>> url_arg = None
>>> config_arg = None
>>> config_env = None
>>> sqlc.get_sessionmaker(config_section, config_prefix, url_arg=url_arg, config_arg=config_arg, config_env=config_env, config_default=config_default)
>>> sessionmaker(class_='Session', bind=Engine(mysql+pymysql://user:***@127.0.0.1:3306/db_default), autoflush=True, autocommit=False, expire_on_commit=True)
```

See the API documentation for lower level functions.
