"""Test that the column info analysis returns the correct data types.
"""
from sqlalchemy_config.orm_code import (
    FileInfo, FileColumn as C
)
from pyaddons import utils
import pytest
import csv
import os


_DELIMITER = "\t"
"""The delimiter of temp output file (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_header(row):
    """Get a dummy header row that is the same length as the given row.

    Parameters
    ----------
    row : `list`
        A row that determines the length of the dummy header.

    Returns
    -------
    header : `list` of `str`
        A dummy header row, this is just the prefix ``column`` along side a
        zero based column number.
    """
    return [f"column{i}" for i in range(len(row))]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _write_data(outfile, data, header=False):
    """Write the data to an output file.

    Parameters
    ----------
    outfile : `str`
        The name of the output file.
    data : `list` of `list` of `any`
        The data to write to file. Each sub-list is a row.
    header : `bool`, optional, default: `NoneType`
        Output a dummy header row to the output file, this is just the prefix
        ``column`` along side a zero based column number.
    """
    with open(outfile, 'wt') as outfh:
        writer = csv.writer(outfh, delimiter=_DELIMITER)
        if header is True:
            writer.writerow(_get_header(data[0]))
        for row in data:
            writer.writerow(row)
    return outfile


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    'has_header,test_file,exp_results',
    (
        (
            False,
            (
                [0, "A", 2, 9, 0.34],
                [1, "B", 1, 8, 1],
                [10, "C", None, 7, 2.45],
                [None, "AA", 2, 6, 0.1],
                [None, "BBBB", 1, 5, 5E-04],
                [33, "CCC", 2, 4, 10001.234],
            ),
            (
                (C.Dtype.INTEGER, True, True, False, 2, 6, 4),
                (C.Dtype.VARCHAR, False, False, False, 4, 6, 6),
                (C.Dtype.BOOLEAN, True, False, False, 1, 6, 5),
                (C.Dtype.INTEGER, False, False, True, 1, 6, 6),
                (C.Dtype.FLOAT, False, False, False, 9, 6, 6),
            ),
        ),
    )
)
def test_file_info(tmpdir, has_header, test_file, exp_results):
    """Test that the file info returns the correct data types.
    """
    outfile = _write_data(
        utils.get_temp_file(dir=str(tmpdir)), test_file, header=has_header
    )

    try:
        fi = FileInfo(outfile, no_header=not has_header)
        fi.set_dtypes()
        for r, c in zip(exp_results, fi.info.columns):
            assert r[0] == c.dtype, "wrong data type"
            assert r[1] == c.nullable, "wrong nullable"
            assert r[2] == c.ascending, "wrong ascending"
            assert r[3] == c.descending, "wrong descending"
            assert r[4] == c.max_len, "wrong max_len"
            assert r[5] == c.ntests, "wrong ntests"
            assert r[6] == c.nvalued_tests, "wrong nvalued_tests"
    finally:
        os.unlink(outfile)
