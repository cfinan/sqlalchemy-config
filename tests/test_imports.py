"""Tests that all the modules in the package can be imported without error.
"""
import pytest
import importlib


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "import_loc",
    (
        'sqlalchemy_config.column_info',
        'sqlalchemy_config.config',
        'sqlalchemy_config.orm_code',
        'sqlalchemy_config.orm_doc',
        'sqlalchemy_config.test_connect',
        'sqlalchemy_config.utils',
    )
)
def test_package_import(import_loc):
    """Test that the modules can be imported.
    """
    importlib.import_module(import_loc, package=None)
