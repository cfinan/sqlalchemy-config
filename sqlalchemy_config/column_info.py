"""Very basic utilities for working out column data types. Currently can define
string, integer and float. It also gives an indication if the column has
missing data i.e. ``""`` and the max (string) length in the column.
"""
# Importing the version number (in __init__.py) and the package name
from sqlalchemy_config import (
    __version__,
    __name__ as pkg_name,
    orm_code
)
from pyaddons import log, utils
from pyaddons.flat_files import header
from tqdm import tqdm
import argparse
import sys
import os
import gzip
import re
import pathlib
import csv
import stdopen
# import pprint as pp

_SCRIPT_NAME = "orm-column-info"
"""The name of the script (`str`)
"""
# Use the module docstring for argparse
_DESC = __doc__
"""The program description (`str`)
"""

# Full length csv files
csv.field_size_limit(sys.maxsize)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API use see
    ``something else``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    try:
        fi = orm_code.FileInfo(
            args.infile, encoding=args.encoding,
            comment=args.comment, skiplines=args.skip_lines,
            delimiter=args.delimiter, no_header=args.no_header,
            header_cols=args.header_columns
        )
        fi.set_dtypes(verbose=log.progress_verbose(args.verbose))
        with stdopen.open(args.outfile, 'wt') as outfile:
            writer = csv.writer(outfile, delimiter="\t")
            header_out = False
            for idx, col in enumerate(fi.info.columns):
                if header_out is False:
                    writer.writerow(
                        [
                            "column_idx", "column_name", "dtype",
                            "max_len", "is_ascending", "is_descending",
                            "is_nullable", "total_rows",
                            "total_not_null_rows"
                        ]
                    )
                    header_out = True

                # dtype = col.dtype("1").__class__.__name__
                writer.writerow(
                    [
                        idx, col.name, col.dtype.value, col.max_len,
                        int(col.ascending), int(col.descending),
                        int(col.nullable), col.ntests, col.nvalued_tests
                    ]
                )
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        log.log_end(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        'infile', type=str,
        help="The path to the input file (can be gzip compressed)"
    )
    parser.add_argument(
        '-o', '--outfile', type=str,
        help="The path to the output file (if not given will default to"
        " STDOUT)"
    )
    parser.add_argument(
        '-d', '--delimiter', type=str, default="\t",
        help="The delimiter of the input file"
    )
    parser.add_argument(
        '-e', '--encoding', type=str, default="UTF8",
        help="The encoding of the input file"
    )
    parser.add_argument(
        '-s', '--skip-lines', type=int, default=0,
        help="The skip this many lines before processing the header"
    )
    parser.add_argument(
        '-c', '--comment', type=str, default="#",
        help="Skip lines that start with this string"
    )
    parser.add_argument(
        '-N', '--no-header', action="store_true",
        help="The input file has no header columns, in which case"
        " column numbers are used as column names"
    )
    parser.add_argument(
        '--header-columns', type=str, nargs="+",
        help="The header column names to use if the input file has no header"
        " columns"
    )
    parser.add_argument(
        '-v', '--verbose',  action="count", default=0,
        help="give more output, -vv will turn on progress monitoring"
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args
