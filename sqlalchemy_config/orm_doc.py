"""Utilities for generating docstrings and documentation from SQLAlchemy ORM
definitions.

This will parse any existing numpy docstring components for the summary and
 Notes sections and re-use them. Only the Parameters are built a-new.

This will add if the column is an Index and if a string then the length
allowed. If it is nullable the parameter data type definition will be
optional. It currently handles: ``sqlalchemy.String``.
``sqlalchemy.Float``, ``sqlalchemy.Numeric``, ``sqlalchemy.Integer``,
``sqlalchemy.SmallInteger``, ``sqlalchemy.BigInteger``,
``sqlalchemy.Text``, ``sqlalchemy.Boolean``, ``sqlalchemy.Date``,
``sqlalchemy.DateTime``.
"""
from pyaddons import utils

try:
    # SQLalchemy >= 1.4
    from sqlalchemy.orm import declarative_base
except ImportError:
    # SQLalchemy <= 1.3
    from sqlalchemy.ext.declarative import declarative_base

# from sqlalchemy.inspection import inspect
from sqlalchemy import (
    String,
    Integer,
    Float,
    Text,
    SmallInteger,
    BigInteger,
    Boolean,
    Numeric,
    Date,
    DateTime
)
from numpydoc.docscrape import NumpyDocString
import textwrap
import stdopen
import importlib.machinery
import importlib.util
import argparse
import re
import inspect
# import pprint as pp


_SCHEMA_PROG_NAME = "orm-doc-schema"
"""The name of the ORM schema program that doubles as the logger name (`str`)
"""
_SCHEMA_DESC = """Schema description restructured text files from ORM objects
and their ``doc`` attribute.

It will output the format an ``.rst`` file with the format for each table:

``<TABLE_NAME>``
``<TABLE DESCRIPTION>``
``<COLUMN_NUMBER>. <COLUMN_NAME> (<DATA_TYPE>(<LENGTH>)) - <DESCRIPTION>``

"""
"""ORM schema program description (`str`)
"""

_DOCSTRING_PROG_NAME = "orm-docstring"
"""The name of the ORM docstring program that doubles as the logger name
(`str`)
"""
_DOCSTRING_DESC = """Create Numpy docstrings with the parameters defined from
the ORM columns and their ``doc`` attribute.

This will parse any existing numpy docstring components for the summary and
 Notes sections and re-use them. Only the Parameters are built a-new.

This will add if the column is an Index and if a string then the length
allowed. If it is nullable the parameter data type definition will be
optional. It currently handles: ``sqlalchemy.String``.
``sqlalchemy.Float``, ``sqlalchemy.Numeric``, ``sqlalchemy.Integer``,
``sqlalchemy.SmallInteger``, ``sqlalchemy.BigInteger``,
``sqlalchemy.Text``, ``sqlalchemy.Boolean``, ``sqlalchemy.Date``,
``sqlalchemy.DateTime``.

Be aware that this will import the file that has been given so be sure to
know where they file came from.
"""
"""ORM docstring program description (`str`)
"""

_API_DOC_PROG_NAME = "orm-api-doc"
"""The name of the ORM API rst documentation program that doubles as the
logger name (`str`)
"""
_API_DESC = """Create an API restructured text documentation from ORM
objects. This just uses the ``.. autoclass:: <modulename>`` with the
``:class-doc-from: class`` flag, to use only the ORM docstrings.

Be aware that this will import the file that has been given so be sure to
know where they file came from.
"""
"""ORM API documentation program description (`str`)
"""

MODULE_NAME = 'orm'
"""The name which is used when importing the ORM module (`str`)
"""
Base = declarative_base()
"""Used to search for ORM classes
`sqlalchemy.ext.declarative import declarative_base.Base`.
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main_orm_schema():
    """The main entry point for the schema documentation script.
    """
    parser = _init_orm_schema_cmd_args()
    args = _parse_cmd_args(parser)

    # Import the ORM module
    orm_module = import_module(args.orm_file)

    # Locate all the ORM classes.
    orm_defs = get_orm_classes(orm_module)

    # Sanity check
    if len(orm_defs) == 0:
        raise IndexError("no ORM clases found")

    # Parse the docstrings and writer to file
    with stdopen.open(args.outfile, 'wt') as outfile:
        # Loop through all the ORM classes
        for n, c in orm_defs:
            outfile.write(
                get_heading("``{0}`` table".format(c.__tablename__))
            )
            outfile.write("\n")
            outfile.write("{0}\n".format(get_table_description(c)))
            outfile.write("\n")
            for c in get_column_descriptions(c):
                outfile.write(f"{c}\n")
            outfile.write("\n")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main_orm_docstring():
    """The main entry point for the ORM docstring script.
    """
    parser = _init_orm_doc_cmd_args()
    args = _parse_cmd_args(parser)

    # Import the ORM module
    orm_module = import_module(args.orm_file)

    # Locate all the ORM classes.
    orm_defs = get_orm_classes(orm_module)

    # Sanity check
    if len(orm_defs) == 0:
        raise IndexError("no ORM clases found")

    # Parse the docstrings and writer to file
    with stdopen.open(args.outfile, 'wt') as outfile:
        # Loop through all the ORM classes
        for n, c in orm_defs:
            # Build the docstring components
            summary, params, notes = get_docstring(
                c, width=args.width, indent=args.indent,
                module_name=args.module_name
            )
            write_docstring(outfile, c, summary, notes, params,
                            indent=args.indent)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main_orm_api_docs():
    """The main entry point for the ORM API documentation script.
    """
    parser = _init_orm_api_doc_cmd_args()
    args = _parse_cmd_args(parser)

    # Import the ORM module
    orm_module = import_module(args.orm_file)

    # Locate all the ORM classes.
    orm_defs = get_orm_classes(orm_module)

    # Sanity check
    if len(orm_defs) == 0:
        raise IndexError("no ORM clases found")

    # Parse the docstrings and writer to file
    with stdopen.open(args.outfile, 'wt') as outfile:
        outfile.write(
            get_heading(f"``{args.module_name}`` module")
        )
        # Loop through all the ORM classes
        for n, c in orm_defs:
            outfile.write("\n")
            full_class = f"{args.module_name}.{n}"
            outfile.write(
                f".. autoclass:: {full_class}\n   :class-doc-from: class\n"
            )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_orm_schema_cmd_args():
    """Use argparse to parse the command line arguments for the ORM to schama
    documentation.

    Returns
    -------
    parser : `argparse.ArgumentParser`
        The arguments expected on the command line.
    """
    return _init_cmd_args(_SCHEMA_DESC)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_orm_doc_cmd_args():
    """Use argparse to parse the command line arguments for the ORM to
    docstring documentation.

    Returns
    -------
    parser : `argparse.ArgumentParser`
        The arguments expected on the command line.
    """
    parser = _init_cmd_args(_DOCSTRING_DESC)
    parser.add_argument(
        '-w', '--width', type=int, default=79,
        help="The width for the docstring lines in characters",
    )
    parser.add_argument(
        '-i', '--indent', type=int, default=0,
        help="The indent in characters"
    )
    parser.add_argument(
        '--module-name', type=str,
        help="The module name for the relationship parameter datatypes",
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_orm_api_doc_cmd_args():
    """Use argparse to parse the command line arguments for the ORM to
    API documentation script.

    Returns
    -------
    parser : `argparse.ArgumentParser`
        The arguments expected on the command line.
    """
    parser = _init_cmd_args(_API_DESC)
    parser.add_argument(
        'module_name', type=str,
        help="The module name for ``.. autoclass::``",
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args(desc):
    """Use argparse to parse the command line arguments.

    Parameters
    ----------
    desc : `str`
        The program description.

    Returns
    -------
    parser : `argparse.ArgumentParser`
        The arguments expected on the command line.
    """
    parser = argparse.ArgumentParser(
        description=desc
    )

    parser.add_argument(
        'orm_file', type=str,
        help="The python module file containing the orm definitions."
    )
    parser.add_argument(
        '-o', '--outfile', type=str,
        help="The output file, if not provided output will be to STDOUT."
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Use argparse to parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The arguments expected on the command line.

    Returns
    -------
    args : `ArgumentParser.args`
        The arguments from parsing the cmd line args.
    """
    args = parser.parse_args()
    args.orm_file = utils.get_full_path(args.orm_file, allow_none=True)
    args.outfile = utils.get_full_path(args.outfile, allow_none=True)

    try:
        utils.test_same_files(
            args.orm_file, args.outfile,
            error_msg="infile and outfile and the same"
        )
    except TypeError:
        if args.outfile is not None:
            raise

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_module(orm_file):
    """Import the orm file as a module so it can be analysed for ORM classes.

    Parameters
    ----------
    orm_file : `str`
        The ORM file to import.

    Returns
    -------
    orm_module : `module`
        The imported module called ``orm``.

    Notes
    -----
    Obviously this is running arbitrary python code so make sure you know what
    is in ``orm_file``.
    """
    loader = importlib.machinery.SourceFileLoader(
        MODULE_NAME, str(orm_file)
    )
    spec = importlib.util.spec_from_loader(MODULE_NAME, loader)
    orm_module = importlib.util.module_from_spec(spec)
    loader.exec_module(orm_module)
    return orm_module


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_orm_classes(orm_module):
    """Extract all the SQLAlchemy ORM classes from the ``orm_module``. This is
    anything that is subclassed from
    ``sqlalchemy.ext.declarative.declarative_base.Base``

    Parameters
    ----------
    orm_module : `module`
        The imported module called ``orm``.

    Returns
    -------
    orm_classes : `list` of `tuple`
        Each tuple has the class name (`str`) and the ORM class.
    """
    orm_defs = []
    for n, c in orm_module.__dict__.items():
        try:
            # TypeError: issubclass() arg 1 must be a class, not sure why it
            # does not work??
            parents = inspect.getmro(c)
        except AttributeError:
            continue
        for p in parents:
            if p.__class__ == Base.__class__ and c.__name__ != Base.__name__:
                orm_defs.append((n, c))
                break
    return orm_defs


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_docstring(orm_class, width=79, indent=0, module_name=None):
    """Get the formatted docstring sections.

    Parameters
    ----------
    orm_class : `class`
        A class inheriting from
        ``sqlalchemy.ext.declarative.declarative_base.Base``.
    width : `int`, optional, default: `79`
        The line width for the output docstring in characters.
    indent : `int`, optional, default: `0`
        How many blank characters should the line be indented by.
    module_name : `str`, optional, default: `NoneType`
        A module name to use for the full path to any relationship classes.

    Returns
    -------
    summary : `list` of `str`, optional, default: `NoneType`
        The summary rows to write.
    parameters : `list` of `tuple`, optional, default: `NoneType`
        Each tuple will have a data type definition at ``[0]`` and a ``list``
        of ``str`` for the description at ``[1]``.
    notes : `list` of `str`, optional, default: `NoneType`
        The notes section rows to write.

    Notes
    -----
    This will parse any existing numpy docstring components for the summary
    and Notes sections and re-use them. Only the Parameters are built a-new.
    """
    # Get the indent characters
    indent = get_indent(indent)

    # The initial indent of the summary line
    initial_indent = f'{indent}"""'

    # Parse any existing numpy docstring components.
    docstring = NumpyDocString(orm_class.__doc__)
    summary = textwrap.wrap(
        " ".join([i.strip() for i in docstring["Summary"]]),
        width=width, initial_indent=initial_indent, subsequent_indent=indent
    )
    notes = textwrap.wrap(
        " ".join([i.strip() for i in docstring["Notes"]]),
        width=width, initial_indent=indent, subsequent_indent=indent
    )

    # Format and build the parameters
    format_params = []
    # print("****** INDENT ********", max(len(indent) * 2, 4))
    param_desc_indent = indent * 2
    if len(param_desc_indent) == 0:
        param_desc_indent = get_indent(4)

    for n, desc in get_parameters(orm_class, module_name=module_name):
        n = textwrap.indent(n, indent)
        desc = textwrap.wrap(
            desc, width=width, initial_indent=param_desc_indent,
            subsequent_indent=param_desc_indent
        )
        format_params.append((n, desc))
    return summary, format_params, notes


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_parameters(orm_class, module_name=None):
    """Get the parameter information from the orm class. The description is
    taken from the ``sqlalchemy.Column.doc`` attribute or the relationships.

    Parameters
    ----------
    orm_class : `class`
        A class inheriting from
        ``sqlalchemy.ext.declarative.declarative_base.Base``.
    module_name : `str`, optional, default: `NoneType`
        A module name to use for the full path to any relationship classes.

    Returns
    -------
    parameters : `list` of `tuple`
        Each tuple will have a data type definition at ``[0]`` and a ``list``
        of ``str`` for the description at ``[1]``.
    """
    module_name = module_name or ""
    if module_name != "":
        module_name = f"{module_name}."

    params = []
    for c in orm_class.__table__.columns:
        param_def, length = get_docstring_datatype(c)
        desc = get_description(c, length)
        params.append((param_def, desc))

    # Now annotate any relationships
    for r in orm_class.__mapper__.relationships:
        dtype = (
            f"{r.target} : `{module_name}{r.mapper.class_.__name__}`"
            ", optional, default: `NoneType`"
        )

        desc = r.doc
        if r.doc is None or r.doc == "":
            desc = \
                f"Relationship with ``{r.mapper.class_.__tablename__}``."
        if not desc.endswith('.'):
            desc = desc + "."
        params.append((dtype, desc))
    return params


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_docstring_datatype(column):
    """Get the numpy docstring datatype definition string based on SQLAlchemy
    column datatypes.

    Parameters
    ----------
    column : `sqlalchemy.Column`
        The SQLAlchemy column object. This should have the ``doc`` attribute
        filled. Empty ``doc`` attributes will insert a default "Undefined"
        string as the description.

    Returns
    -------
    data_type_str : `str`
        The numpy datatype definition string.
    description : `str`
        the data description.

    Notes
    -----
    This will add if the column is an Index and if a string then the length
    allowed. If it is nullable the parameter data type definition will be
    optional. It currently handles: ``sqlalchemy.String``.
    ``sqlalchemy.Float``, ``sqlalchemy.Numeric``, ``sqlalchemy.Integer``,
    ``sqlalchemy.SmallInteger``, ``sqlalchemy.BigInteger``,
    ``sqlalchemy.Text``, ``sqlalchemy.Boolean``, ``sqlalchemy.Date``,
    ``sqlalchemy.DateTime``.
    """
    dtype = column.type.__class__
    base_param = f'{column.name} : '
    length = None
    if dtype == String:
        base_param = f'{base_param}`str`'
        length = str(column.type.length)
    elif dtype in [Float, Numeric]:
        base_param = f'{base_param}`float`'
    elif dtype in [Integer, SmallInteger, BigInteger]:
        base_param = f'{base_param}`int`'
    elif dtype == Text:
        base_param = f'{base_param}`str`'
        length = "text"
    elif dtype == Boolean:
        base_param = f'{base_param}`bool`'
    elif dtype == Date:
        base_param = f'{base_param}`date.date`'
    elif dtype == DateTime:
        base_param = f'{base_param}`date.datetime`'

    if column.nullable is True:
        default_value = column.default
        if column.default is None:
            default_value = 'NoneType'

        base_param = f'{base_param}, optional, default: `{default_value}`'

    return base_param, length


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_description(column, length):
    """Get the docstring parameter description.

    Parameters
    ----------
    column : `sqlalchemy.Column`
        The SQLAlchemy column object. This should have the ``doc`` attribute
        filled. Empty ``doc`` attributes will insert a default "Undefined"
        string as the description.
    length : `int` or `NoneType`
        The datatype length (if column is a string or text).

    Returns
    -------
    description : `str`
        The parameter description.
    """
    desc = column.doc
    try:
        if not desc.endswith('.'):
            desc = desc + '.'
    except AttributeError:
        if desc is not None:
            raise
        desc = f'The ``{column.name}`` column.'

    space = " " * bool(len(desc))
    if column.index is True:
        desc = f'{desc}{space}This is indexed.'

    if len(column.foreign_keys) > 0:
        fk = ", ".join(
            [f"``{i.target_fullname}``" for i in column.foreign_keys]
        )
        desc = f'{desc}{space}Foreign key to {fk}.'

    if length is not None:
        desc = re.sub(r'\.$', '', desc)
        desc = f'{desc}{space}(length {length}).'
    return desc


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def write_docstring(outfile, class_name, summary=None, notes=None,
                    parameters=None, indent=0):
    """Write the docstring to the output location.

    Parameters
    ----------
    outfile : `File`
        A file like object to write to.
    class : `class`
        The class where the docstring components were derived from.
    summary : `list` of `str`, optional, default: `NoneType`
        The summary rows to write.
    notes : `list` of `str`, optional, default: `NoneType`
        The notes section rows to write.
    parameters : `list` of `tuple`, optional, default: `NoneType`
        Each tuple will have a data type definition at ``[0]`` and a ``list``
        of ``str`` for the description at ``[1]``.
    """
    summary = summary or ['{0}"""'.format(get_indent(indent))]
    notes = notes or []
    parameters = parameters or []

    outfile.write(f"# {class_name.__name__}\n")

    # Output the summary row, there will always be one.
    for i in summary:
        outfile.write(f"{i}\n")

    if len(parameters) > 0:
        # Gap
        outfile.write("\n")

        outfile.write(get_heading("Parameters", indent=indent))
        for dtype, desc in parameters:
            outfile.write(f"{dtype}\n")
            for line in desc:
                outfile.write(f"{line}\n")

    if len(notes) > 0:
        # Gap
        outfile.write("\n")

        outfile.write(get_heading("Notes", indent=indent))
        for line in notes:
            outfile.write(f"{line}\n")
    # Final triple quote
    outfile.write('{0}"""\n'.format(get_indent(indent)))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_indent(indent, char=" "):
    """Get the indent characters.

    Parameters
    ----------
    indent : `int`
        The width of the indent.
    char : `str`, optional, default: ` `
        The character that forms the indent.

    Returns
    -------
    indent_chars : `str`
        The indent characters.
    """
    return char * indent


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_heading(name, indent=0):
    """Get an underlined and potentially indented docstring heading.

    Parameters
    ----------
    name : `str`
        The heading name.
    indent : `int`, optional, default: `0`
        The number of characters to indent the heading.

    Returns
    -------
    heading : `str`
        The formatted heading string.
    """
    indent =  " " * indent
    iname = textwrap.indent(name, indent)
    iunderline = textwrap.indent("-" * len(name), indent)
    return f"{iname}\n{iunderline}\n"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_table_description(orm_class):
    """
    """
    # Parse any existing numpy docstring components.
    docstring = NumpyDocString(orm_class.__doc__)
    summary = " ".join([i.strip() for i in docstring["Summary"]])
    notes = " ".join([i.strip() for i in docstring["Notes"]])
    desc = re.sub(r'\s+', ' ', f"{summary} {notes}".strip())
    if not desc.endswith('.'):
        desc = f"{desc}."
    return desc


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_column_descriptions(orm_class):
    """
    Parameters
    ----------
    orm_class : `class`
        A class inheriting from
        ``sqlalchemy.ext.declarative.declarative_base.Base``.

    Returns
    -------
    summary : `list` of `str`, optional, default: `NoneType`
        The summary rows to write.
    parameters : `list` of `tuple`, optional, default: `NoneType`
        Each tuple will have a data type definition at ``[0]`` and a ``list``
        of ``str`` for the description at ``[1]``.
    notes : `list` of `str`, optional, default: `NoneType`
        The notes section rows to write.

    Notes
    -----
    ``<COLUMN_NUMBER>. <COLUMN_NAME> (<DATA_TYPE>(<LENGTH>)) - <DESCRIPTION>``
    """
    params = []
    for idx, c in enumerate(orm_class.__table__.columns, 1):
        params.append(
            "{0}. ``{1}`` (``{2}``) - {3}".format(
                idx, c.name, str(c.type), get_description(c, None)
            )
        )
    return params
