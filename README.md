# Getting Started with sqlalchemy-config

__version__: `0.4.4a0`

The sqlalchemy-config package is a toolkit to handle database configuration parameters that are stored in a config file. This can locate, read, and extract the config parameters and create a sessionmaker object from them that is bound to the engine object.

There is [online](https://cfinan.gitlab.io/sqlalchemy-config/index.html) documentation for sqlalchemy-config.

## Installation instructions
At present, sqlalchemy-config is undergoing development and no packages exist yet on PyPi. Therefore it is recommended that it is installed in either of the two ways listed below.

### Installation using pip
You can install using pip from the root of the cloned repository, first clone and cd into the repository root:

```
git clone git@gitlab.com:cfinan/sqlalchemy-config.git
cd sqlalchemy-config
```

Install the dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then install using pip
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the repository. The difference with this is that you can just to a `git pull` to update, or switch branches without re-installing:
```
python -m pip install -e .
```

### Installation using conda
I maintain a conda package in my personal conda channel. To install this please run:

```
conda install -c cfin -c conda-forge sqlalchemy-config
```

### Conda dependencies
There are also conda yaml environment files in `./resources/conda/env` that have the same contents as `requirements.txt` but for conda packages, so all the pre-requisites. I use this to install all the requirements via conda and then install the package as an editable pip install.

However, if you find these useful then please use them. There are Conda environments for Python v3.8, v3.9 and v3.10.

## Usage TL;DR
sqlalchemy-config is designed to handle database connection inputs potentially coming from different sources. Such as a URL or file (for SQLite) being specified on the command line, or a path to a config file containing the parameters being specified as a command line arg, or in a environment variable or the route of the home directory.

The config files used in the example below have a similar structure to this see [SQLAlchemy engine from config](https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.engine_from_config):


```
[mydb_mysql]
# Make sure this is only readable by you
# Make sure password is URL encoded, see: sqlalchemy_config.encode_password
db.url = mysql+pymysql://user:password@127.0.0.1:3306/db_cfg_arg
```

```python
>>> import sqlalchemy_config as sqlc
>>> import os

>>> # This represents input fromt he command line, of a single file that will be inperpreted as SQLite
>>> url_arg = "mydb.db"

>>> # This is the section in the config file (ini) where we will find any database connection parameters
>>> config_section = 'mydb_mysql'

>>> # This is the prefix from any connection parameters in under config_section
>>> config_prefix = 'db.'

>>> # This represents a default location for config files containing parameters
>>> config_file = os.path.join(os.environ['HOME'], "mydbs_def.cnf")

>>> The url will take priority and the file is interpreted as SQLite connection
>>> sqlc.get_new_sessionmaker(config_section, conn_prefix=config_prefix, config_arg=config_file)
>>> sessionmaker(class_='Session', bind=Engine(sqlite:///mydb.db), autoflush=True, autocommit=False, expire_on_commit=True)

# The URL argument can also be a fully formed connection url
>>> url_arg = "mysql+pymysql://user:password@127.0.0.1:3306/db_url_arg"
>>> sqlc.get_new_sessionmaker(url_arg)
>>> sessionmaker(class_='Session', bind=Engine(mysql+pymysql://user:***@127.0.0.1:3306/db_url_arg), autoflush=True, autocommit=False, expire_on_commit=True)
```

The original ``get_sessionmaker`` function is shown below, note that this is deprecated.

```
[mydb_mysql]
# Make sure this is only readable by you
# Make sure password is URL encoded, see: sqlalchemy_config.encode_password
db.url = mysql+pymysql://user:password@127.0.0.1:3306/db_cfg_arg
```

```python
>>> import sqlalchemy_config as sqlc
>>> import os

>>> # This represents input fromt he command line, of a single file that will be inperpreted as SQLite
>>> url_arg = "mydb.db"

>>> # This is the section in the config file (ini) where we will find any database connection parameters
>>> config_section = 'mydb_mysql'

>>> # This is the prefix from any connection parameters in under config_section
>>> config_prefix = 'db.'

>>> # This represents an environemnt variable name in your ~/.bashrc that has the path to a config file
>>> config_env = "MYDBS"

>>> # This represents a default location for config files containing parameters
>>> config_default = os.path.join(os.environ['HOME'], "mydbs_def.cnf")

>>> The url will take priority and the file is interpreted as SQLite connection
>>> sqlc.get_sessionmaker(config_section, config_prefix, url_arg=url_arg, config_arg=config_arg, config_env=config_env, config_default=config_default)
>>> sessionmaker(class_='Session', bind=Engine(sqlite:///mydb.db), autoflush=True, autocommit=False, expire_on_commit=True)

# The URL argument can also be a fully formed connection url
>>> url_arg = "mysql+pymysql://user:password@127.0.0.1:3306/db_url_arg"
>>> sqlc.get_sessionmaker(config_section, config_prefix, url_arg=url_arg, config_arg=config_arg, config_env=config_env, config_default=config_default)
>>> sessionmaker(class_='Session', bind=Engine(mysql+pymysql://user:***@127.0.0.1:3306/db_url_arg), autoflush=True, autocommit=False, expire_on_commit=True)

# URL is not supplied so look at any conffig file command line arguments and use the section and prefix information
>>> url_arg = None
>>> sqlc.get_sessionmaker(config_section, config_prefix, url_arg=url_arg, config_arg=config_arg, config_env=config_env, config_default=config_default)
>>> sessionmaker(class_='Session', bind=Engine(mysql+pymysql://user:***@127.0.0.1:3306/db_cfg_arg), autoflush=True, autocommit=False, expire_on_commit=True)

# url and config arg not supplied so fall back to any environment variables
>>> url_arg = None
>>> config_arg = None
>>> sqlc.get_sessionmaker(config_section, config_prefix, url_arg=url_arg, config_arg=config_arg, config_env=config_env, config_default=config_default)
>>> sessionmaker(class_='Session', bind=Engine(mysql+pymysql://user:***@127.0.0.1:3306/db_env_var), autoflush=True, autocommit=False, expire_on_commit=True)

# url, config and env are missing so fall back to the default location
>>> url_arg = None
>>> config_arg = None
>>> config_env = None
>>> sqlc.get_sessionmaker(config_section, config_prefix, url_arg=url_arg, config_arg=config_arg, config_env=config_env, config_default=config_default)
>>> sessionmaker(class_='Session', bind=Engine(mysql+pymysql://user:***@127.0.0.1:3306/db_default), autoflush=True, autocommit=False, expire_on_commit=True)
```

See the API documentation for lower level functions.

## Change log

### version `0.2.0a0`
* API - added a simpler sessionmaker function to the config module
* SCRIPTS - added scripts moved from pyaddons ``orm-doc-api``, ``orm-doc-schema``, ``orm-docstring``.
* SCRIPTS - added new ORM code producing script ``orm-create-src``.
* BUILD - removed Python v3.7
* BUILD - Updated requirements for new scripts
* DOCS - Added new script/API docs

### version `0.2.0a1`
* BUILD - added missing requirement.

### version `0.2.0a2`
* API - fixed description crash for columns with undefined doc attribute

### version `0.2.0a3`
* DOCS - added change log
* DOCS - Fixed missing columns in ``orm-create-src`` documentation.

### version `0.3.0a0`
* API - added better Python type conversion functions in Column objects.
* API - added a set of casting functions in the `sqlalchemy.type_cast` module.
* BUILD - added pytest calls from conda build.

### version `0.3.1a0`
* API - added `get_new_sessionmaker` function and deprecated `get_sessionmaker`.
* DOCS - Added documentation for the test connection API.

### version `0.4.0a0`
* BUGFIX - Fixed config permissions checking added flexibility of perms arguments.
* BUGFIX - Fixed bug in exist check from config file.
* API - `get_new_sessionmaker` function, added option to raise on database exists.
* API - Added data type URLs
* API - Updated imports for SQLAlchemy 2
* TESTS - Added config tests.
* BUILD - Updated requirements to use SQLAlchemy >=2
* BUILD - Removed unused example directory and conftest.py

### version `0.4.1a0`
* BUGFIX - Fixed import bug for SQLAlchemy 2.

### version `0.4.2a0`
* BUGFIX - Fixed bug in ORM class detection for orm-doc-*.

### version `0.4.3a0`
* SCRIPT - `orm-test-connect` , improved the reporting of table query errors.

### version `0.4.4a0`
* SCRIPT - `orm-create-src` , fixed reflection errors due to SQLAlchemy 2.
