.. sqlalchemy-config documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sqlalchemy-config
============================

`sqlalchemy-config <https://gitlab.com/cfinan/sqlalchemy-config>`_ is a package for handling SQLAlchemy connection parameters stored in a configuration (``.ini``) file. In addition, there are some handy scripts for creating ORM code and documentation.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   scripts
   api

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
