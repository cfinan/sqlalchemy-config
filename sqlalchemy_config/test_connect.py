"""Test an ORM module to see if it is representative of the database and
visa versa. This will also issue queries against all of the database tables
defined in the ORM to make sure they all complete.
"""
# Importing the version number (in __init__.py) and the package name
from sqlalchemy_config import (
    __version__,
    __name__ as pkg_name
)
from pyaddons import log

# For handing data base configuration options within config files
# Delete if not needed
from sqlalchemy_config import config as cfg
from sqlalchemy import inspect

try:
    # SQLAlchemy <= 1.3
    from sqlalchemy.ext.declarative.api import DeclarativeMeta
except ImportError:
    # SQLAlchemy >= 1.4
    from sqlalchemy.orm import DeclarativeMeta

from tqdm import tqdm
from tqdm.contrib.logging import logging_redirect_tqdm
import argparse
import sys
import os
import importlib
# import pprint as pp


# The name of the script
_SCRIPT_NAME = "orm-test-connect"
"""The name of the script (`str`)
"""
# Use the module docstring for argparse
_DESC = __doc__
"""The program description (`str`)
"""

# Here are some useful variables for SQLAlchemy
try:
    _DEFAULT_CONFIG = os.path.join(os.environ['HOME'], '.db.cnf')
    """The default fallback path for the config file, root of the home
    directory (`str`)
    """
except KeyError:
    # HOME environment variable does not exist for some reason
    _DEFAULT_CONFIG = None

_DEFAULT_SECTION = 'db'
"""The default section name in an uni config file that contains SQLAlchemy
connection parameters (`str`)
"""
_DEFAULT_PREFIX = 'db.'
"""The default prefix name in an ini config section each SQLAlchemy connection
parameter should be prefixed with this (`str`)
"""
KNOWN_MISSING_DB_TAB = ['sqlite_stat1']
"""Allowed table names that will not appear in ORMs (`list`, of `str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API use see
    ``sqlalchemy_config.test_connect.test_connection``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    # Use logger.info to issue log messages
    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    # Get a sessionmaker to create sessions to interact with the
    # database
    sm = cfg.get_new_sessionmaker(
        args.db, conn_prefix=_DEFAULT_PREFIX,
        config_arg=args.config, config_env=None,
        config_default=_DEFAULT_CONFIG, exists=True
    )
    # Get the session to query/load
    session = sm()

    orm_module = None
    if os.path.exists(args.module):
        # The module is a file path
        spec = importlib.util.spec_from_file_location('orm', args.module)
        orm_module = importlib.util.module_from_spec(spec)
        sys.modules[spec.name] = orm_module
        spec.loader.exec_module(orm_module)
    else:
        # The module is an import string for an installed module
        orm_module = importlib.import_module(args.module, package=None)

    try:
        test_connection(session, orm_module, verbose=args.verbose)
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        session.close()
        log.log_end(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        'db',
        type=str,
        help="An SQLALchemy connection string or a section in a configuration"
        " file containing the connection information. You can use this to"
        " prevent giving passwords on the command line. The default config "
        "file location is ``~/.db.ini``. You can change this with the "
        "``-config`` argument."
    )
    parser.add_argument(
        'module',
        type=str,
        help="Either the path to a module file or a import string. This will "
        "be imported and tested against the database connection."
    )
    parser.add_argument(
        '--config', type=str, default=None,
        help="An alternating database configuration file location."
    )
    parser.add_argument(
        '-v', '--verbose',  action="count",
        help="give more output, use -vv for progress monitoring"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_connection(session, orm_module, verbose=False):
    """Test a database schema against an ORM module to see if they are
    compatible.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A database session. Must return the engine when get_bind() is called.
    orm_module : `Module`
        The ORM module that has been loaded into the Python environment.
    verbose : `bool`, optional, default: `False`
        Tell the user what I am doing.

    Notes
    -----
    This will compare tables and columns between the database represented by
    the ``session`` and the schema as defined in the ``orm_module``. It will
    also issue simple fetch queries against each table (limited to 10 rows).
    Any table/column differences will be logged to STDERR, any query errors
    will be SQLAlchemy exceptions. Please note, this does not check for
    column data types being compatible.
    """
    logger = log.retrieve_logger(_SCRIPT_NAME, verbose=True)

    # Inspect the database and get the tables and columns
    inspector = inspect(session.get_bind())

    db_tables = dict()
    for table_name in inspector.get_table_names():
        columns = dict()
        for column in inspector.get_columns(table_name):
            columns[column['name']] = column
        db_tables[table_name] = columns

    # Get the ORM classes from the orm module
    orm_tables = dict()
    orm_classes = []
    for i in dir(orm_module):
        if not i.startswith('__'):
            obj = getattr(orm_module, i)

            if isinstance(obj, DeclarativeMeta):
                columns = dict()
                try:
                    for c in obj.__table__.columns:
                        columns[c.name] = c
                    orm_classes.append(obj)
                    orm_tables[obj.__table__.name] = columns
                except AttributeError:
                    pass

    # Find ORM tables/column not present in the database
    missing_orm_tables, missing_orm_columns = _match_dicts(
        orm_tables, db_tables
    )

    # Find DB tables/column not present in the ORM
    missing_db_tables, missing_db_columns = _match_dicts(
        db_tables, orm_tables
    )

    # Remove known tables
    _remove_knowns(missing_db_tables, KNOWN_MISSING_DB_TAB)

    # Output any table/column errors
    errors = _output_errors(
        logger, missing_orm_tables, "** ORM Tables missing in the DB:"
    )
    errors |= _output_errors(
        logger, missing_orm_columns, "** ORM columns missing in the DB:"
    )
    errors |= _output_errors(
        logger, missing_db_tables, "** DB tables missing in the ORM:"
    )
    errors |= _output_errors(
        logger, missing_db_columns, "** DB columns missing in the ORM:"
    )

    query_errors = 0
    # Issue the queries
    with logging_redirect_tqdm(loggers=[logger]):
        for c in tqdm(orm_classes, desc="[info] running queries",
                      unit=" queries", disable=not verbose):
            try:
                [i for i in session.query(c).limit(10)]
            except Exception as e:
                query_errors += 1
                logger.error(f"problem with query for {c}")
                logger.error(f"Error: {e.__class__.__name__}: {e.args[0]}")

    if query_errors > 1:
        logger.error(f"Problems running queries: {query_errors} errors")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _remove_knowns(source_list, knowns):
    """Remove known table/column exclusions from a list of missing
    tables/columns.

    Parameters
    ----------
    source_list : `list` of `str`
        The missing tables.
    knowns : `list` of `str`
        The table names that are known to be missing.
    """
    for i in knowns:
        try:
            source_list.pop(source_list.index(i))
        except ValueError:
            if i != 'sqlite_stat1':
                raise


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _output_errors(logger, table_data, msg):
    """Output missing table errors.

    Parameters
    ----------
    logger : `Logger`
        A logger to issue error messages.
    table_data : `list` of `str`
        A list of missing tables.
    msg : `str`
        An error message appropriate for the missing tables.

    Returns
    -------
    errors : `bool`
        `True` if error messages were output, `False` if not.
    """
    errors = False
    if len(table_data) > 0:
        errors = True
        logger.error(msg)
        for i in table_data:
            logger.error(i)
    return errors


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _match_dicts(source, target):
    """Determine which tables and columns in the source are not present in the
    target.

    Parameters
    ----------
    source : `dict`
        The source table names (keys) and columns names (values).
    target : `dict`
        The target table names (keys) and columns names (values).

    Returns
    -------
    missing_source_tables : `list` of `str`
        This is a list of tables from the source that are not present in the
        target.
    missing_source_columns : `list` of `str`
        This is a list of columns from the source that are not present in the
        target. Notes that if a table is missing then it's columns are not
        checked so will not appear in this list. Therefore, this list only
        lists columns that are missing in tables that are present.
    """
    missing_source_tables = []
    missing_source_columns = []

    for tname, columns in source.items():
        try:
            target_cols = target[tname]
        except KeyError:
            missing_source_tables.append(tname)
            continue

        for cname, column in columns.items():
            try:
                target_cols[cname]
            except KeyError:
                missing_source_columns.append(f"{tname}.{cname}")
    return missing_source_tables, missing_source_columns


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
